#menu.py for general HdM stuff

print ("MENUPY: hdm menu.py loaded")
import nuke
from hdm_scripts import rpSubmit
from hdm_scripts import rpSubmitLowPower
from hdm_scripts import convertFileKnobsToUNC


#### Color
nuke.knobDefault("colorManagement", "OCIO")

######## PREFERENCES
prefs=nuke.toNode("preferences")
prefs['AutoSaveTime'].setValue(120)
prefs['diskCacheGB'].setValue(20)
prefs['maxPanels'].setValue(4)
prefs['LocalizationSizeLimit'].setValue(50)

#HdM topmennu
nukeMenu = nuke.menu('Nuke')
hdmMenu = nukeMenu.addMenu("HDM")
hdmMenu.addCommand("Submit to RenderPal V2 - Full Farm", "rpSubmit.rpSubmit()","Shift+R")
##rs
hdmMenu.addCommand("Submit to RenderPal V2 - VFX Low Power", "rpSubmitLowPower.rpSubmit()","Shift+Alt+R")
hdmMenu.addCommand("File knobs to UNC", "convertFileKnobsToUNC.convertFileKnobs()")

#HdM toolbar
nuke.pluginAddPath('hdm_gizmos')
toolbar=nuke.menu("Nodes")
hdmTBMenu=toolbar.addMenu("HdM",icon="HdM_Menu_neu.png")

hdmTBMenu.addCommand("HdM_unspill_v01", 'nuke.createNode("HdM_unspill_v01")', icon="HdM_unspill_neu.png")
hdmTBMenu.addCommand( "HdM_SliceTool", "nuke.createNode('HdM_SliceTool')", icon="HdM_SliceTool_neu.png")


#mmColorTarget
nuke.pluginAddPath('mmColorTarget')
hdmTBMenu.addCommand("mmColorTarget", 'nuke.createNode("mmColorTarget")', icon="mmColorTarget.png")
