# -------------------------------------------------------------------------------------------------------
# Nuke Submitter Script for the RenderPal V2 Submitter V2.12.3
# For support send email to: support@shoran.de
# -------------------------------------------------------------------------------------------------------
# Copyright (c) Shoran Software. All rights reserved.
# -------------------------------------------------------------------------------------------------------
#
# INSTRUCTIONS:
#  Note: Installation instructions can be found in the accompanying Install.txt
#
#  This script requires a copy of the RenderPal V2 Submitter. If you haven't got one yet, go to
#  http://www.renderpal.com to download the submitter for your system.
#
#  Before using this script, run the RenderPal V2 Submitter once; this will register an environment
#  variable so that the script can automatically locate the submitter.
#
# TROUBLESHOOTING:
#  If the submitter has been properly installed, this script does not require any configuration and
#  works right out-of-the-box. If you run into problems, however, try the following:
#    1. Run the submitter and make sure that it is working correctly
#    2. In the submitter, configure the RenderPal V2 Server address and login
#    3. On some systems, a reboot might be needed before the submitter can be found
#    4. If the submitter still cannot be found, you can modify the global submitter path (see below)
#       accordingly
#    5. Otherwise, contact us at support@shoran.de
#
# -------------------------------------------------------------------------------------------------------

import os, platform, tempfile, configparser, subprocess
import nuke

# The global submitter path; leave empty to let the script read the path from
# the system environment. Only enter the full path to the submitter if necessary
g_rpSubmitterLocation = ""

# -- The rest of this script should not be modified --

g_rpSubmitterExe	= "RpSubmitter"
g_rpSubmitterPath	= ""

g_rpSettingsFile	= ""

if platform.system().lower() == "windows":
	g_rpSubmitterExe = "RpSubmitter.exe"

# -- Helper functions

def rpPrint(msg):	
	print ("rpSubmit: " + msg)
	
def rpError(msg):
	nuke.error(msg)
	nuke.message(msg)
	
	return False
	
def rpFormatIniValue(val):
	val = str(val)	
	val = val.replace("\\", "/")
	val = val.replace(";", ",")
	
	return val

# -- Core functions

def rpReadSubmitterPath():
	global g_rpSubmitterLocation, g_rpSubmitterExe, g_rpSubmitterPath
	
	if g_rpSubmitterLocation != "":
		fullPath = os.path.join(g_rpSubmitterLocation, g_rpSubmitterExe)
		
		if not os.path.isfile(fullPath):
			g_rpSubmitterLocation = ""

	if (g_rpSubmitterLocation == "") and ("RP_SUBMITTER_DIR" in os.environ):
		g_rpSubmitterLocation = os.environ["RP_SUBMITTER_DIR"]
	
	g_rpSubmitterPath = os.path.normpath(os.path.join(g_rpSubmitterLocation, g_rpSubmitterExe))

	return (g_rpSubmitterPath != "") and os.path.isfile(g_rpSubmitterPath)
	
def rpExportSettings():
	global g_rpSettingsFile
	
	g_rpSettingsFile	= os.path.join(tempfile.gettempdir(), "RpSubmitterData.ini")
	
	cfgParser			= ConfigParser.SafeConfigParser()	
	writeNodes			= []
	
	try:
		#render all write nodes that are enabled
		# for node in nuke.allNodes("Write"):
		# 	if not node["disable"].getValue():
		# 		writeNodes.append(node.name())
		#render all write nodes that are selected
		for node in nuke.selectedNodes():
			if node.Class() == 'Write':
				writeNodes.append(node.name())
	except:
		pass
		
	# -- Job count
	
	jobCount = len(writeNodes)
	
	cfgParser.add_section("Main")
	cfgParser.set("Main", "JobCount", rpFormatIniValue(jobCount))
		
	# -- The scene
	
	sceneFile = nuke.value("root.name")
	
	cfgParser.add_section("Scene")
	cfgParser.set("Scene", "scene", rpFormatIniValue(sceneFile))
	
	# -- The renderer
	
	cfgParser.add_section("JobSettings")
	#cfgParser.set("JobSettings", "nj_renderer", rpFormatIniValue("NukeX/10.5v4")) # changed to NukeX/10.5v4 by Peter Ruhrmann 4.12.2017

	cfgParser.set("JobSettings", "nj_renderer", rpFormatIniValue("NukeX/13.0.v5")) # changed to Nuke 11 to be able to render on the 043 pool

	# -- Pool

	cfgParser.set("JobSettings", "nj_pools", rpFormatIniValue("vfx_redshift")) # Pool-Section added by Peter Ruhrmann 4.12.2017 (currently not working)
	

	# Job Color		
	#jobcolor = "-nj_color "50,128,50"""	

	#sectioncolor = "Additional"
	#addcommands = -nj_color "0,0,0"
	
	#cfgParse.add_section("Additional.addcommands")
	#cfgParser.set("Additional.addcommands", "nj_color", rpFormatIniValue("50,128,50")) # added default Job Color 

	# -- Render settings
	
	cfgParser.add_section("RenderSettings")
	
	try:
		startFrame = nuke.Root().knob("first_frame").getValue()
		endFrame = nuke.Root().knob("last_frame").getValue()
		
		if startFrame != 0 and endFrame != 0:
			if startFrame != endFrame:
				cfgParser.set("RenderSettings", "frames", str(startFrame) + "-" + str(endFrame))
			else:
				cfgParser.set("RenderSettings", "frames", str(startFrame))
	except:
		pass

	# -- Job settings		
	
	for idx, curNode in enumerate(writeNodes):
		jobName = os.getenv('USERNAME') + " - " + os.path.basename(sceneFile) + " - Node: " + curNode
		
		section	= "GeneralSettings." + str(idx)
		cfgParser.add_section(section)
		cfgParser.set(section, "nj_name", rpFormatIniValue(jobName))
				
		try:
			section	= "RenderSettings." + str(idx)
			cfgParser.add_section(section)
			
			fileName = nuke.filename(nuke.toNode(curNode))
			cfgParser.set(section, "outdir", os.path.dirname(fileName))
			cfgParser.set(section, "outfile", os.path.basename(fileName))
						
			if nuke.toNode(curNode).knob("use_limit").getValue():				
				startFrame	= nuke.toNode(curNode).knob("first").getValue()
				endFrame	= nuke.toNode(curNode).knob("last").getValue()
	   
				if startFrame != 0 and endFrame != 0:
					if startFrame != endFrame:
						cfgParser.set(section, "frames", str(startFrame) + "-" + str(endFrame))
					else:
						cfgParser.set(section, "frames", str(startFrame))
		except:
			pass
		
		section	= "Additional." + str(idx)
		cfgParser.add_section(section)
		cfgParser.set(section, "writenode", rpFormatIniValue(curNode))
	
	# -- All exported, write the .ini
	
	with open(g_rpSettingsFile, "wb") as cfgFile:
		cfgParser.write(cfgFile)
	
	return True
	
def rpPrepareSubmission():
	global g_rpSettingsFile
	
	sceneName = nuke.value("root.name")
	
	if sceneName == None or sceneName == "":
		return rpError("Please save your Nuke script before submitting it.")
		
	if rpExportSettings():
		rpPrint("  Settings exported to: " + g_rpSettingsFile)
	else:
		return False
	
	return True
	
def rpLaunchSubmitter():
	global g_rpSubmitterPath, g_rpSettingsFile
	
	rpPrint("Launching RenderPal V2 Submitter...")
	subprocess.Popen([g_rpSubmitterPath, "--group", "Nuke", "--import", g_rpSettingsFile, "--delete-ini"], shell=True, stdin=None, stdout=None, stderr=None, close_fds=True)	
	rpPrint("  Done")
	
# -- Renderer specific helper functions

def rpCreateOutputDir():
	fn		= nuke.filename(nuke.thisNode())
	dn		= os.path.dirname(fn)
	osdir	= nuke.callbacks.filenameFilter(dn)
	
	try:
		os.makedirs (osdir)
	except:
		pass     

# -- Main function

def rpSubmit():
	nuke.scriptSave() #this is important as the onScriptSave repaths everything to UNC

	rpPrint("Initializing...")		
	
	if rpReadSubmitterPath():
		rpPrint("  Submitter location: " + g_rpSubmitterPath)
	else:
		return rpError("RenderPal V2 Submitter not found. Make sure that there is a copy installed on this system and that you have run it at least once.")
	
	if rpPrepareSubmission():
		rpLaunchSubmitter()			
		
	return True

nuke.addBeforeRender(rpCreateOutputDir, nodeClass = "Write")
