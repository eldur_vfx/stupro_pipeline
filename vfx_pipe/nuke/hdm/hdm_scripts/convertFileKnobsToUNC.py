import posixpath
import nuke
import nukescripts

basePath = '//vfx-nas01/vfx2'

def nodeWithFile():
    fileKnobNodes = [i for i in nuke.allNodes(recurseGroups=True) if nukescripts.searchreplace.__NodeHasFileKnob(i)]
    return fileKnobNodes

def convertFileKnobs():
    for node in nodeWithFile():
        absPath = str(nuke.filename(node))
        absPath = posixpath.normpath(absPath)
        if absPath.startswith("Z:/"):
            newPath = absPath.replace('Z:/', "", 1).lstrip("/")
            node['file'].setValue(posixpath.join(basePath, newPath))   
        elif not absPath.startswith("Z:/") and not absPath.startswith(basePath):
            newPath = absPath.lstrip("/")
            node['file'].setValue(posixpath.join(basePath, newPath))
    nuke.scriptSave()