# from scripts.saveNewVersion import saveNewVersion
import os
import json
from show_scripts.saveNewCompVersion import saveNewCompVersion

print("MENUPY: show menu.py loaded")

#################################################################################################
PIPE_PIPELINEPATH = os.getenv('PIPE_PIPELINEPATH')
PIPE_PROJECTPATH = os.getenv('PIPE_PROJECTPATH')

with open(os.path.join(PIPE_PIPELINEPATH, "PIPELINE_CONSTANTS.json"), "r") as read_file:
    PIPELINE_CONSTANTS = json.load(read_file)

SG_PROJECTNAME = PIPELINE_CONSTANTS['SG_PROJECTNAME']
#################################################################################################

######## PREFERENCES
prefs = nuke.toNode("preferences")
prefs['autoLocalCachePath'].setValue(PIPE_PROJECTPATH.replace("\\", "/"))
prefs['AutoSaveTime'].setValue(60)
prefs['diskCacheGB'].setValue(20)
prefs['maxPanels'].setValue(4)
# prefs['selectedGPUDeviceIndex'].setValue(1) #for GTX970

######## Save new version
nukeMenu = nuke.menu('Nuke')
# nuke.addOnScriptSave(saveNewVersion.checkScriptName) CHECK PURPOSE AGAIN
# modifying the File - Save New Comp Version via custom script
fileMenu = nukeMenu.menu('File')
newVersion = fileMenu.findItem('Save New Comp Version')
newVersion.setScript('saveNewCompVersion.saveNewCompVersion()')
saveAs = fileMenu.findItem('Save Comp As...')
saveAs.setScript(
    'nuke.message(\'Please use the Save New Comp Version dialog instead, in order to maintain pipeline integrity! - Greetings Pipeline Dev\')\nnuke.scriptSaveAs()')

######## Top menu
showMenu = nukeMenu.addMenu(SG_PROJECTNAME)

# Load Footage
from show_scripts.createNewScript import kbCreateNewScript
showMenu.addCommand('Start your SHOT', kbCreateNewScript.createNewBasicScript, "alt+z")
showMenu.addCommand('Start your PREP Shot', kbCreateNewScript.createNewPrepScript)
showMenu.addCommand('Start your COMP Shot', kbCreateNewScript.createNewCompScript)



# Create CustomContactSheet
from show_scripts.createNewScript import kbCreateContactSheet
show_contactSheetMenu = showMenu.addMenu('Contact Sheet Utility')
show_contactSheetMenu.addCommand('Create ContactSheet', kbCreateContactSheet.createContactSheet)
show_contactSheetMenu.addCommand('Update ContactSheet to EXR', kbCreateContactSheet.updateContactSheetExr)
show_contactSheetMenu.addCommand('Update ContactSheet to JPG', kbCreateContactSheet.updateContactSheetJpg)

# Scripts
#from show_scripts import denoice
#showMenu.addCommand("Arnold Denoiser", "denoice.start()", "Ctrl+Shift+D")
# broken with Nuke13. 2.11.2021 Peter Ruhrmann

from show_scripts import knob_scripter
showMenu.addCommand('tools/Open Floating Knob Scripter', knob_scripter.showKnobScripter)
showMenu.addCommand('tools/Update KnobScripter Context', knob_scripter.updateContext).setVisible(True)

# convienince
showMenu.addSeparator()
from show_scripts import readFromWrite
showMenu.addCommand('Read from Write', 'readFromWrite.ReadFromWrite()')

from show_scripts import GoToDirectory
showMenu.addCommand('Go To Directory', "GoToDirectory.GoToDirectory()")

######## Gizmos
SHOW_GIZMOPATH = os.path.join(PIPE_PIPELINEPATH, "nuke", "stupro", "show_gizmos").replace("\\", "/")
show_gizMenu = nuke.menu('Nodes').addMenu(SG_PROJECTNAME)
gizmoList = os.listdir(SHOW_GIZMOPATH)
for gizmo in gizmoList:
    command = SHOW_GIZMOPATH + "/" + gizmo
    show_gizMenu.addCommand(gizmo.split(".")[0], "nuke.createNode('{}')".format(command))

# Backdrop
from show_scripts.F_Backdrop import F_Backdrop
nuke.pluginAddPath('./show_scripts/F_Backdrop/icons')
nukescripts.autoBackdrop = F_Backdrop.autoBackdrop  # Original backdrop function replacement
showMenu.addCommand('nodes/Backdrop', 'F_Backdrop.autoBackdrop()', 'alt+b', 'Backdrop.png')
