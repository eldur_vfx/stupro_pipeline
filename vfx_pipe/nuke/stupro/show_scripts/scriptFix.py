import nuke
import nukescripts
import posixpath


def repathToUNC():
    basePath = '//vfx-nas01/vfx2'
    fileKnobNodes = [i for i in nuke.allNodes(recurseGroups=True) if nukescripts.searchreplace.__NodeHasFileKnob(i)]
    for node in fileKnobNodes:
        absPath = str(nuke.filename(node))
        absPath = posixpath.normpath(absPath)
        if absPath == "." or absPath == "//vfx-nas01/vfx2/None":  # file knob should be empty
            node['file'].setValue("")
        else:
            if absPath.startswith("Z:/"):  # repath from z to UNC
                newPath = absPath.replace('Z:/', "", 1).lstrip("/")
                node['file'].setValue(posixpath.join(basePath, newPath))
            elif not absPath.startswith("Z:/") and not absPath.startswith(basePath):  # repair non unc, non zpaths
                newPath = absPath.lstrip("/")
                node['file'].setValue(posixpath.join(basePath, newPath))


def versionKnobChecker():  # catch and fix inconsitencies between version knob and script version
    try:
        versionHelper = nukescripts.VersionHelper(nuke.root().name())
        if versionHelper._currentV != nuke.root()[
            'version'].getValue():  # catch and fix inconsitencies between version knob and script version
            nuke.root()['version'].setValue(versionHelper._currentV)
    except ValueError as e:
        nuke.message("Unable to save new comp version:\n%s" % str(e))
        return

    # def fixColorManagement():


#     #Colormanagment
#     nuke.root()['colorManagement'].setValue('OCIO')
#     nuke.root()['colorManagement'].setEnabled(False)

#     nuke.root()['OCIO_config'].setValue('custom')
#     nuke.root()['OCIO_config'].setEnabled(False)

#     nuke.root()['customOCIOConfigPath'].setValue('//vfx-nas01/vfx2/_pipeline/master/hdmvfx-pipeline/pipeline/ocio/configs/filmlight/config.ocio')
#     nuke.root()['customOCIOConfigPath'].setEnabled(False)

#     nuke.knobDefault('Root.monitorLut', 'TCAM 100 nits office/sRGB: ~2.2 Gamma - Rec.709')

def runScriptFix_onLoad():
    pass


def runScriptFix_onSave():
    versionKnobChecker()
    nuke.root()['colorManagement'].setValue('OCIO')
