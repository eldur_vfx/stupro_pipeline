def createReadNodes():
    rootNode = nuke.root()
    code = rootNode['shotcode'].getValue()

    seq_types = ['mp', 'cp', 'cg']  # main plate, clean plate, element plate
    for type in seq_types:
        if type is 'cg':
            seq_name = ''
            plate_path = os.path.join(PIPE_SHOTPATH, code, '3D', 'cg', '_export', 'img_fin')
            min = 10000
            max = 0
            if os.path.exists(plate_path):
                newest_ver = 1
                newest_folder = ''
                for folder in os.listdir(plate_path):
                    version = re.findall("(_v)(\d{3})", folder)
                    if version > newest_ver:
                        newest_ver = version
                        newest_folder = folder

                plate_path = os.path.join(plate_path, newest_folder)
                for img in os.listdir(plate_path):
                    seq_name = img
                    frame = int(img.split('.')[1])
                    if frame < min:
                        min = frame
                        print(min)
                    if frame > max:
                        max = frame
                seq_name = seq_name.replace(str(max), '####')
                file_path = os.path.join(plate_path, seq_name)
                file_path = file_path.replace('\\', '/')
                read_node = nuke.nodes.Read(file=file_path)
                read_node["first"].setValue(int(min))
                read_node["last"].setValue(int(max))
                dot = nuke.nodes.Dot()
                dot.setInput(0, read_node)
        else:
            seq_name = code + '_' + type + '.####.exr'
            plate_path = os.path.join(PIPE_SHOTPATH, code, 'src', type)
            min = 10000
            max = 0
            if os.path.exists(plate_path):
                for img in os.listdir(plate_path):
                    frame = int(img.split('.')[1])
                    if frame < min:
                        min = frame
                        print(min)
                    if frame > max:
                        max = frame

                file_path = os.path.join(plate_path, seq_name)
                file_path = file_path.replace('\\', '/')
                read_node = nuke.nodes.Read(file=file_path)
                read_node["first"].setValue(int(min))
                read_node["last"].setValue(int(max))
                dot = nuke.nodes.Dot()
                dot.setInput(0, read_node)

    dot = nuke.createNode('Dot')
    dot.setYpos(500)
    write_node = nuke.nodes.Write()
    write_node.setInput(0, dot)


# todo: this part is currently a bit confusing because of errors when rendering the plates. Should look like the createReadNodes function above
def updateReadNodes():
    rootNode = nuke.root()
    code = rootNode['shotcode'].getValue()

    if code != '':
        seq_types = ['mp', 'cp', 'cg', 'ep', 'prp']  # main plate, clean plate, cg plate, lighting ref, prep plate
        for type in seq_types:

            # CG Plates
            if type is 'cg':
                seq_name = ''
                cg_path = os.path.join(PIPE_SHOTPATH, code, '3D', 'cg', '_export', 'img-fin')
                min = 10000
                max = 0
                if os.path.exists(cg_path):
                    newest_ver = 1
                    newest_folder = ''
                    if os.listdir(cg_path):
                        for version_folder in os.listdir(cg_path):
                            version = re.findall("(_v)(\d{3})", version_folder)
                            if version > newest_ver:
                                newest_ver = version
                                newest_folder = version_folder
                        shot_number = code.split('_')
                        shotcam_folder_name = 'shotcam_' + shot_number[1]
                        shotcam_path = os.path.join(cg_path, newest_folder, shotcam_folder_name)
                        for layer_folder in os.listdir(shotcam_path):
                            layer_folder_path = os.path.join(shotcam_path, layer_folder)
                            for img in os.listdir(layer_folder_path):
                                seq_name = img
                                frame = int(img.split('.')[1])
                                if frame < min:
                                    min = frame
                                    print(min)
                                if frame > max:
                                    max = frame
                            seq_name = seq_name.split('.')[0] + '.####.' + seq_name.split('.')[2]

                            layer = layer_folder.split('_')
                            if len(layer) > 2:
                                layer_name = layer[0].upper() + '_' + layer[1].upper()
                            elif len(layer) == 2:
                                layer_name = layer[0].upper()
                            # print(layer_name)
                            file_path = os.path.join(shotcam_path, layer_folder, seq_name)
                            file_path = file_path.replace('\\', '/')
                            n = nuke.toNode(layer_name)
                            if n:
                                n.knob('file').setValue(file_path)
                                n["first"].setValue(int(min))
                                n["last"].setValue(int(max))

            # PRP Plates

            # Lighting Ref Plate

            # CleanPlate and MainPlate in one
            elif type is 'cp':
                seq_name = code + '_cp.####.exr'
                plate_path = os.path.join(PIPE_SHOTPATH, code, 'source', type)
                min = 10000
                max = 0
                if os.path.exists(plate_path):
                    for img in os.listdir(plate_path):
                        frame = int(img.split('.')[1])
                        if frame < min:
                            min = frame
                            print(min)
                        if frame > max:
                            max = frame

                    file_path = os.path.join(plate_path, seq_name)
                    file_path = file_path.replace('\\', '/')
                    n = nuke.toNode(type.upper())
                    if n:
                        n.knob('file').setValue(file_path)
                        n["first"].setValue(int(min))
                        n["last"].setValue(int(max))
            elif type is 'ep':
                seq_name = code + '_lightRef.####.exr'
                plate_path = os.path.join(PIPE_SHOTPATH, code, 'source', type, 'LightningRef')
                min = 10000
                max = 0
                if os.path.exists(plate_path):
                    for img in os.listdir(plate_path):
                        frame = int(img.split('.')[1])
                        if frame < min:
                            min = frame
                            print(min)
                        if frame > max:
                            max = frame
                    file_path = os.path.join(plate_path, seq_name)
                    file_path = file_path.replace('\\', '/')
                    n = nuke.toNode(type.upper())
                    if n:
                        n.knob('file').setValue(file_path)
                        n["first"].setValue(int(min))
                        n["last"].setValue(int(max))
            elif type is 'prp':
                prp_path = os.path.join(PIPE_SHOTPATH, code, '2D', 'comp', '_export', 'img-fin')
                max_version = 0
                plate_folder = ""
                if os.path.exists(prp_path):
                    if os.listdir(prp_path):
                        for version_folder in os.listdir(prp_path):
                            parts = version_folder.split('_')
                            if len(parts) == 5:
                                if parts[2] == type:
                                    version = int(parts[-2][1:4])
                                    if version > max_version:
                                        max_version = version
                                        plate_folder = version_folder
                        if max_version != 0:
                            seq_name = plate_folder + '.####.exr'
                            plate_path = os.path.join(prp_path, plate_folder)
                            min = 10000
                            max = 0
                            if os.path.exists(plate_path):
                                for img in os.listdir(plate_path):
                                    if len(img.split('.')) > 2:
                                        frame = int(img.split('.')[1])
                                        if frame < min:
                                            min = frame
                                            print(min)
                                        if frame > max:
                                            max = frame

                                file_path = os.path.join(plate_path, seq_name)
                                file_path = file_path.replace('\\', '/')
                                n = nuke.toNode(type.upper())
                                if n:
                                    n.knob('file').setValue(file_path)
                                    n["first"].setValue(int(min))
                                    n["last"].setValue(int(max))

            elif type is 'prp':
                prp_path = os.path.join(PIPE_SHOTPATH, code, '2D', 'prp', '_export', 'img-fin')
                max_version = 0
                plate_folder = ""
                if os.path.exists(prp_path):
                    if os.listdir(prp_path):
                        for version_folder in os.listdir(prp_path):
                            parts = version_folder.split('_')
                            if len(parts) == 5:
                                if parts[2] == type:
                                    version = int(parts[-2][1:4])
                                    if version > max_version:
                                        max_version = version
                                        plate_folder = version_folder
                        if max_version != 0:
                            seq_name = plate_folder + '.####.exr'
                            plate_path = os.path.join(prp_path, plate_folder)
                            min = 10000
                            max = 0
                            if os.path.exists(plate_path):
                                for img in os.listdir(plate_path):
                                    if len(img.split('.')) > 2:
                                        frame = int(img.split('.')[1])
                                        if frame < min:
                                            min = frame

                                        if frame > max:
                                            max = frame

                                file_path = os.path.join(plate_path, seq_name)
                                file_path = file_path.replace('\\', '/')
                                n = nuke.toNode(type.upper())
                                if n:
                                    n.knob('file').setValue(file_path)
                                    n["first"].setValue(int(min))
                                    n["last"].setValue(int(max))


            elif type is 'mp':
                seq_name = code + '_' + type + '.####.exr'
                plate_path = os.path.join(PIPE_SHOTPATH, code, 'source', type)
                min = 10000
                max = 0
                if os.path.exists(plate_path):
                    for img in os.listdir(plate_path):
                        frame = int(img.split('.')[1])
                        if frame < min:
                            min = frame

                            print(min)
                        if frame > max:
                            max = frame
                    setGlobalRange(min, max)
                    file_path = os.path.join(plate_path, seq_name)
                    file_path = file_path.replace('\\', '/')
                    n = nuke.toNode(type.upper())
                    if n:
                        n.knob('file').setValue(file_path)
                        n["first"].setValue(int(min))
                        n["last"].setValue(int(max))