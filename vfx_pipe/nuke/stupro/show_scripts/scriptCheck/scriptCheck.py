# check script stage, compare with shotgun
from pprint import pprint
import nuke
import sgtk

eng = sgtk.platform.current_engine()
shotContext = eng.context.entity


def querryShot_fromContext(shotContext):
    if shotContext is not None:
        filter = [
            ['code', 'is', shotContext['name']],
            ['id', 'is', shotContext['id']]
        ]
        returnFields = ['project.Project.name', 'code', 'sg_cut_in', 'sg_cut_out', 'sg_aperture', 'sg_focallength',
                        'sg_shutter', 'sg_fps', 'sg_editversion', 'sg_neutralgrade_cdl', 'sg_setdatasheet']
        shotDict = eng.shotgun.find_one("Shot", filter, returnFields)
        print(shotDict)
        return shotDict
    else:
        print("ERROR: script is not executed in shot context")
        return None


# test
shotDict = querryShot_fromContext(shotContext)


def setCutInfo():
    code = nuke.root()['shotcode'].getValue()
    if code:
        current_shot = sg.find("CutItem", filters=[["code", "is", code]],
                               fields=["cut_item_in", "cut_item_out", "edit_in", "edit_out"])
        cut_in = current_shot[0].get("cut_item_in")
        cut_out = current_shot[0].get("cut_item_out")
        edit_in = current_shot[0].get("edit_in")
        edit_out = current_shot[0].get("edit_out")

        if (edit_in is None) and (edit_out is None):
            nuke.Root()['first_frame'].setValue(cut_in)
            nuke.Root()['last_frame'].setValue(cut_out)
        else:
            nuke.Root()['first_frame'].setValue(edit_in)
            nuke.Root()['last_frame'].setValue(edit_out)


def script_sanityCheck(shotDict):
    # retrieve data from current script
    script = {'frange': (nuke.root()['first_frame'].value(), nuke.root()['last_frame'].value()),
              'fps': nuke.root()['fps'].value(),
              'projectname': nuke.root()['project'].value(),
              'code': nuke.root()['shotcode'].value(),
              'neutralgrade': nuke.root()['neutralgrade_cdl'].value()}

    # extract data from dictionary
    sg = {'frange': (shotDict['sg_cut_in'], shotDict['sg_cut_out']),
          'fps': shotDict['sg_fps'],
          'projectname': shotDict['project.Project.name'],
          'code': shotDict['code'],
          'neutralgrade': shotDict['sg_neutralgrade_cdl']}

    # compare both
    compareDict = {key: [script[key], sg[key], script[key] == sg[key]] for key in sg}


def querryShot_fromString(SG_PROJECTNAME, shotcode):
    filter = [
        ['project.Project.name', 'is', projectname],
        ['code', 'is', shotcode]
    ]
    returnFields = ['sg_cut_in', 'sg_cut_out', 'sg_aperture', 'sg_focallength', 'sg_shutter', 'sg_fps',
                    'sg_editversion', 'sg_neutralgrade_cdl', 'sg_setdatasheet']
