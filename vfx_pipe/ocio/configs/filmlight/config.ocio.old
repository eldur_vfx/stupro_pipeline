ocio_profile_version: 1
search_path: transforms 
strictparsing: true
luma: [0.2126, 0.7152, 0.0722]

description: TCS for OCIO 0.4

roles:
  color_picking: "FilmLight: Linear - E-Gamut"
  color_timing: "FilmLight: T-Log - E-Gamut"
  compositing_Log: "FilmLight: T-Log - E-Gamut"
  data: "Filmlight: Linear - E-Gamut"
  default: "Filmlight: Linear - E-Gamut"
  matte_paint: "Filmlight: Linear - E-Gamut"
  reference: "FilmLight: Linear - XYZ"
  scene_linear: "Filmlight: Linear - E-Gamut"
  texture_paint: "Filmlight: Linear - E-Gamut"

displays:
  TCAM 100 nits office:
    - !<View> {name: "sRGB: ~2.2 Gamma - Rec.709", colorspace: "sRGB: ~2.2 Gamma - Rec.709"}
  TCAM 100 nits video dim:
    - !<View> {name: "Rec1886: 2.4 Gamma - Rec.709", colorspace: "Rec1886: 2.4 Gamma - Rec.709"}
  TCAM 48 nits cinema dark:
    - !<View> {name: "DCI: 2.6 Gamma - P3D65", colorspace: "DCI: 2.6 Gamma - P3D65"}
  TCAM 48 nits cinema dark - MWP D65:
    - !<View> {name: "DCI: 2.6 Gamma - XYZ", colorspace: "DCI: 2.6 Gamma - XYZ"}
  TCAM 100 nits videoWide dim:
    - !<View> {name: "Rec1886: 2.4 Gamma - Rec.2020", colorspace: "Rec1886: 2.4 Gamma - Rec.2020"}
  TCAM 100 nits officeWide:
    - !<View> {name: "AdobeRGB: ~2.2 Gamma - AdobeRGB", colorspace: "AdobeRGB: ~2.2 Gamma - AdobeRGB"}
  TCAM 1000 nits videoWide dim - OOTF 1.2:
    - !<View> {name: "Rec2100: HLG - 2020 - 1000 nits", colorspace: "Rec2100: HLG - 2020 - 1000 nits - OOTF 1.2"}
  TCAM 1000 nits videoWide dim:
    - !<View> {name: "Rec2100: PQ - 2020 - 1000 nits", colorspace: "Rec2100: PQ - 2020 - 1000 nits"}
  TCAM 4000 nits videoWide dim:
    - !<View> {name: "Rec2100: PQ - 2020 - 4000 nits", colorspace: "Rec2100: PQ - 2020 - 4000 nits"}
  USE ONLY FOR CHECKING COMPS:
    - !<View> {name: LowContrast, colorspace: "FilmLight: T-Log - E-Gamut"}
  USE ONLY FOR COMPARING RENDERS:
    - !<View> {name: ReferenceLINEAR, colorspace: "FilmLight: Linear - E-Gamut"}

colorspaces:
  - !<ColorSpace>
    name: "FilmLight: Linear - E-Gamut"
    bitdepth: 32f
    family: SceneReferred
    description: linear scene referred compositing space
    to_reference: !<FileTransform> {src: EGamut_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "FilmLight: T-Log - E-Gamut"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: TLog_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: EGamut_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ACES: Linear - AP0"
    bitdepth: 32f
    family: SceneReferred
    description: linear scene referred compositing space
    to_reference: !<FileTransform> {src: AP0_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ACES: Linear - AP1"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: AP1_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ACES: ACEScc - AP1"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: ACEScc_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: AP1_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ACES: ACEScct - AP1"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: ACEScct_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: AP1_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ARRI: Linear - WG"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: ARRIWG_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ARRI: LogC - WG"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: LogC_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: ARRIWG_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "BMD: Film4.6K - URSA-Gamut"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: BMDFilm46k_to_linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: BMD_URSA_V1_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "BMD: Film4.6K - URSA-Gamut V2"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: BMDFilm46k_V2_to_linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: BMD_URSA_V2_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "CGI: Linear - Rec.709"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: Rec709_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "CGI: CineonLog - Rec.709"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: CineonLog_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: Rec709_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "RED: Linear - REDWideGamut"
    bitdepth: 32f
    family: SceneReferred
    description: linear scene referred compositing space
    to_reference: !<FileTransform> {src: REDWG_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "RED: Log 3G10 - REDWideGamut"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: REDLog3G10_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: REDWG_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Sony: Linear - SGamut3"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: SGamut3_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Sony: SLog3 - SGamut3"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: SLog3_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: SGamut3_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Sony: Linear - SGamut3.Cine"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: SGamut3Cine_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Sony: SLog3 - SGamut.Cine"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: SLog3_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: SGamut3Cine_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Panasonic: Linear - VGamut"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: VGamut_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Panasonic: VLog - SGamut.Cine"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: VLog_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: VGamut_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "sRGB: Linear - Rec709"
    family: SceneReferred
    bitdepth: 32f
    description: for HDRs with sRGB-Primaries
    to_reference: !<FileTransform> {src: Rec709_2_XYZ.spimtx}


  - !<ColorSpace>
    name: "FilmLight: Linear - XYZ"
    bitdepth: 32f
    family: SceneReferred
    description: linear reference space
    allocation: lg2
    allocationvars: [-8, 5, 0.00390625]

  - !<ColorSpace>
    name: "sRGB: ~2.2 Gamma - Rec.709"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_sRGB_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: sRGB_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "Rec1886: 2.4 Gamma - Rec.709"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_Rec1886_709_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: Rec1886_709_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "DCI: 2.6 Gamma - P3D65"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_P3D65_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: P3D65_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "DCI: 2.6 Gamma - XYZ"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_DCIXYZ_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: DCIXYZ_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}


  - !<ColorSpace>
    name: "Rec1886: 2.4 Gamma - Rec.2020"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_Rec1886_2020_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: Rec1886_2020_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "AdobeRGB: ~2.2 Gamma - AdobeRGB"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_AdobeRGB_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: AdobeRGB_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "Rec2100: PQ - 2020 - 1000 nits"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_PQ2020_1000_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: PQ2020_1000_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "Rec2100: PQ - 2020 - 4000 nits"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_PQ2020_4000_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: PQ2020_4000_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "Rec2100: HLG - 2020 - 1000 nits - OOTF 1.2"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_HLG2020_1000_TCAM.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: HLG2020_1000_2_TLog_TCAM.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}


