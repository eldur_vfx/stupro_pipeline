ocio_profile_version: 1
search_path: transforms 
strictparsing: true
luma: [0.2126, 0.7152, 0.0722]

description: TCSv2 for OCIO v1.2

roles:
  color_picking: "FilmLight: Linear - E-Gamut"
  color_timing: "FilmLight: T-Log - E-Gamut"
  compositing_Log: "FilmLight: T-Log - E-Gamut"
  data: "Filmlight: Linear - E-Gamut"
  default: "Filmlight: Linear - E-Gamut"
  matte_paint: "Filmlight: Linear - E-Gamut"
  reference: "FilmLight: Linear - XYZ"
  scene_linear: "Filmlight: Linear - E-Gamut"
  texture_paint: "Filmlight: Linear - E-Gamut"

displays:
  TCAMv2 100 nits office:
    - !<View> {name: "sRGB Display: 2.2 Gamma - Rec.709", colorspace: "sRGB Display: 2.2 Gamma - Rec.709"}
  TCAMv2 100 nits video dim:
    - !<View> {name: "Rec1886: 2.4 Gamma - Rec.709", colorspace: "Rec1886: 2.4 Gamma - Rec.709"}
  TCAMv2 48 nits cinema dark:
    - !<View> {name: "DCI: 2.6 Gamma - P3D65", colorspace: "DCI: 2.6 Gamma - P3D65"}
  TCAMv2 48 nits cinema dark - MWP D65:
    - !<View> {name: "DCI: 2.6 Gamma - XYZ", colorspace: "DCI: 2.6 Gamma - XYZ"}
  TCAMv2 100 nits videoWide dim:
    - !<View> {name: "Rec1886: 2.4 Gamma - Rec.2020", colorspace: "Rec1886: 2.4 Gamma - Rec.2020"}
  TCAMv2 100 nits officeWide:
    - !<View> {name: "AdobeRGB: 2.2 Gamma - AdobeRGB", colorspace: "AdobeRGB: 2.2 Gamma - AdobeRGB"}
  TCAMv2 1000 nits videoWide dim - OOTF 1.2:
    - !<View> {name: "Rec2100: HLG - 2020 - 1000 nits", colorspace: "Rec2100: HLG - 2020 - 1000 nits - OOTF 1.2"}
  TCAMv2 1000 nits videoWide dim:
    - !<View> {name: "Rec2100: PQ - 2020 - 1000 nits", colorspace: "Rec2100: PQ - 2020 - 1000 nits"}
  TCAMv2 4000 nits videoWide dim:
    - !<View> {name: "Rec2100: PQ - 2020 - 4000 nits", colorspace: "Rec2100: PQ - 2020 - 4000 nits"}
  USE ONLY FOR CHECKING COMPS:
    - !<View> {name: LowContrast, colorspace: "FilmLight: T-Log - E-Gamut"}
  USE ONLY FOR COMPARING RENDERS:
    - !<View> {name: ReferenceLINEAR, colorspace: "FilmLight: Linear - E-Gamut"}

colorspaces:
  - !<ColorSpace>
    name: "FilmLight: Linear - E-Gamut"
    bitdepth: 32f
    family: SceneReferred
    description: linear scene referred compositing space
    to_reference: !<FileTransform> {src: EGamut_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "FilmLight: T-Log - E-Gamut"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: TLog_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: EGamut_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ACES: Linear - AP0"
    bitdepth: 32f
    family: SceneReferred
    description: linear scene referred compositing space
    to_reference: !<FileTransform> {src: AP0_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "ACEScg: Linear - AP1"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: AP1_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "ACEScc: ACEScc - AP1"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: ACEScc_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: AP1_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ACEScct: ACEScct - AP1"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: ACEScct_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: AP1_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "ARRI: Linear - WG"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: ARRIWG_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "ARRI: LogC - WG"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: LogC_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: ARRIWG_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "CGI: Linear - Rec.709"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: Rec709_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "CGI: CineonLog - Rec.709"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: CineonLog_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: Rec709_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "RED: Linear - REDWideGamut"
    bitdepth: 32f
    family: SceneReferred
    description: linear scene referred compositing space
    to_reference: !<FileTransform> {src: REDWG_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "RED: Log 3G10 - REDWideGamut"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: REDLog3G10_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: REDWG_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Sony: Linear - SGamut3"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: SGamut3_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "Sony: SLog3 - SGamut3"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: SLog3_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: SGamut3_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Sony: Linear - SGamut3.Cine"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: SGamut3Cine_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "Sony: SLog3 - SGamut.Cine"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: SLog3_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: SGamut3Cine_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "Panasonic: Linear - VGamut"
    bitdepth: 32f
    family: SceneReferred
    to_reference: !<FileTransform> {src: VGamut_2_XYZ.spimtx}
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "Panasonic: VLog - VGamut"
    family: SceneReferred
    bitdepth: 32f
    description: Grading Space
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: VLog_2_Linear.spi1d, interpolation: linear}
        - !<FileTransform> {src: VGamut_2_XYZ.spimtx}

  - !<ColorSpace>
    name: "FilmLight: Linear - XYZ"
    bitdepth: 32f
    family: SceneReferred
    description: linear reference space
    allocation: lg2
    allocationvars: [-10, 7, 0.0056065625]

  - !<ColorSpace>
    name: "sRGB Display: 2.2 Gamma - Rec.709"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_sRGB_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: sRGB_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "Rec1886: 2.4 Gamma - Rec.709"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_Rec1886_709_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: Rec1886_709_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "DCI: 2.6 Gamma - P3D65"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_P3D65_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: P3D65_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "DCI: 2.6 Gamma - XYZ"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_DCIXYZ_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: DCIXYZ_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}


  - !<ColorSpace>
    name: "Rec1886: 2.4 Gamma - Rec.2020"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_Rec1886_2020_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: Rec1886_2020_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "AdobeRGB: 2.2 Gamma - AdobeRGB"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_AdobeRGB_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: AdobeRGB_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "Rec2100: PQ - 2020 - 1000 nits"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_PQ2020_1000_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: PQ2020_1000_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "Rec2100: PQ - 2020 - 4000 nits"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_PQ2020_4000_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: PQ2020_4000_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}

  - !<ColorSpace>
    name: "Rec2100: HLG - 2020 - 1000 nits - OOTF 1.2"
    family: DisplayReferred
    bitdepth: 32f
    from_reference: !<GroupTransform>
      children:
        - !<ColorSpaceTransform> {src: "FilmLight: Linear - XYZ", dst: "FilmLight: T-Log - E-Gamut"}
        - !<FileTransform> {src: TLog_2_HLG2020_1000_TCAMv2.cub, interpolation: tetrahedral}
    to_reference: !<GroupTransform>
      children:
        - !<FileTransform> {src: HLG2020_1000_2_TLog_TCAMv2.cub, interpolation: tetrahedral}
        - !<ColorSpaceTransform> {src: "FilmLight: T-Log - E-Gamut", dst: "FilmLight: Linear - XYZ"}