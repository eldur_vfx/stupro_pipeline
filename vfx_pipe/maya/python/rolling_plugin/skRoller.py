import sys
import maya.OpenMaya as om
import maya.OpenMayaMPx as omMPx
import pymel.core as pm
 
nodeName = "skRayCast"
nodeId = om.MTypeId(0x919198)
 
 
class RayCast(omMPx.MPxNode):
    GROUNDMESH = om.MObject()
    OBJECTMESH = om.MObject()
    RAYSOURCE = om.MObject()
    OBJECTPOS = om.MObject()
    MAXSEARCHDISTANCE = om.MObject()
    ENVELOPE = om.MObject()
    TIME = om.MObject()
 
    OUTPOSITION = om.MObject()
 
    def __init__(self):
        omMPx.MPxNode.__init__(self)
        self.savePos = [0, 0, 0]
        self.oldTime = 0.0

    def get_point(self, mesh, distance, raySourcePosition, raySourceDirection):

        meshFn = mesh
        hitFace = None
        hitPoints = om.MFloatPointArray()

        idsSorted = False
        testBothDirections = False
        faceIds = None
        triIds = None
        accelParams = None
        hitRayParam = None
        hitTriangle = None
        hitBary1 = None
        hitBary2 = None
        sortHits = True

        maxParamPtr = distance

        hit = meshFn.allIntersections(raySourcePosition, raySourceDirection, faceIds, triIds, idsSorted, om.MSpace.kWorld, maxParamPtr, testBothDirections, accelParams, sortHits, hitPoints, hitRayParam, hitFace, hitTriangle, hitBary1, hitBary2)

        return hit, hitPoints
        

    def compute(self, plug, dataBlock):
        if plug == RayCast.OUTPOSITION:
            groundMeshPlug = om.MPlug(self.thisMObject(), RayCast.GROUNDMESH)
            objectMeshPlug = om.MPlug(self.thisMObject(), RayCast.OBJECTMESH)
            #meshPlug.isChild()
 
            raySourceMatrixHandle = dataBlock.inputValue(RayCast.RAYSOURCE)
            objectPosHandle = dataBlock.inputValue(RayCast.OBJECTPOS)
            raySourceMatrix = om.MTransformationMatrix(raySourceMatrixHandle.asMatrix())
            objectPosMatrix = om.MTransformationMatrix(objectPosHandle.asMatrix())
            maxSearchDistance = dataBlock.inputValue(RayCast.MAXSEARCHDISTANCE)
            outposHandle = dataBlock.outputValue(RayCast.OUTPOSITION)
            maxDistanceHandle = maxSearchDistance.asFloat()
            envelope = dataBlock.inputValue(RayCast.ENVELOPE)
            envelopeHandle = envelope.asFloat()
            time = dataBlock.inputValue(RayCast.TIME)
            timeHandle = time.asFloat()
 
            raySourcePosition = raySourceMatrix.getTranslation(4)
            objectPos = objectPosMatrix.getTranslation(4)
            directionVector = om.MVector(0, -abs(maxDistanceHandle), 0)
             
            tempMatrix = om.MTransformationMatrix(raySourceMatrix)
            tempMatrix.addTranslation(directionVector, 2)
            tempPos = tempMatrix.getTranslation(4)
             
            raySourceDirection = om.MFloatVector(tempPos - raySourcePosition)
            raySourceDirection.normalize()
            raySourcePosition = om.MFloatPoint(raySourcePosition)

            if groundMeshPlug.isConnected() and objectMeshPlug.isConnected() and envelopeHandle == 1:

                ground_mesh = om.MFnMesh(dataBlock.inputValue(RayCast.GROUNDMESH).asMesh())
                object_mesh = om.MFnMesh(dataBlock.inputValue(RayCast.OBJECTMESH).asMesh())

                ground_hit, ground_hit_point = self.get_point(ground_mesh, maxDistanceHandle, raySourcePosition, raySourceDirection)
                object_hit, object_hit_point = self.get_point(object_mesh, maxDistanceHandle, raySourcePosition, raySourceDirection)
    
                if ground_hit and object_hit:

                    ground_hit_point = ground_hit_point[0]

                    object_hit_point_furthest = object_hit_point[object_hit_point.length()-1]

                    difference_vector = object_hit_point_furthest - ground_hit_point
                    object_displacement = om.MFloatVector(objectPos) - difference_vector

                    outposHandle.set3Float(float(raySourcePosition[0]), float(object_displacement[1]), float(raySourcePosition[2]))
                elif ground_hit:
                    ground_hit_point = ground_hit_point[0]
                    outposHandle.set3Float(float(raySourcePosition[0]), float(ground_hit_point[1]), float(raySourcePosition[2]))


            else:
                outposHandle.set3Float(float(raySourcePosition[0]), 0.0, float(raySourcePosition[2]))
                self.oldTime = timeHandle
            dataBlock.setClean(plug)
 
 
def nodeCreator():
    return omMPx.asMPxPtr(RayCast())
 
 
def nodeInit():
    tAttr = om.MFnTypedAttribute()
    RayCast.GROUNDMESH = tAttr.create("groundMesh", "gMesh", om.MFnData.kMesh)
    tAttr.setStorable(1)
    tAttr.setKeyable(1)

    tAttr = om.MFnTypedAttribute()
    RayCast.OBJECTMESH = tAttr.create("objectMesh", "oMesh", om.MFnData.kMesh)
    tAttr.setStorable(1)
    tAttr.setKeyable(1)
 
    mAttr = om.MFnMatrixAttribute()
    RayCast.RAYSOURCE = mAttr.create("raySourceMatrix", "rSMx")
    mAttr.setStorable(1)
    mAttr.setKeyable(1)

    mAttr = om.MFnMatrixAttribute()
    RayCast.OBJECTPOS = mAttr.create("objectPos", "oPos")
    mAttr.setStorable(1)
    mAttr.setKeyable(1)
 
    numAttr = om.MFnNumericAttribute()
    RayCast.MAXSEARCHDISTANCE = numAttr.create("maxSearchDistance", "mSD", om.MFnNumericData.kFloat, 999.0)
    numAttr.setStorable(1)
    numAttr.setKeyable(1)
     
    numAttr = om.MFnNumericAttribute()
    RayCast.ENVELOPE = numAttr.create("envelope", "e", om.MFnNumericData.kFloat, 0.0)
    numAttr.setStorable(1)
    numAttr.setKeyable(1)
     
    numAttr = om.MFnNumericAttribute()
    RayCast.TIME = numAttr.create("time", "t", om.MFnNumericData.kFloat, 0.0)
    numAttr.setStorable(1)
    numAttr.setKeyable(1)
 
    numAttr = om.MFnNumericAttribute()
    RayCast.OUTPOSITION = numAttr.createPoint("outPosition", "op")
    numAttr.setStorable(1)
    numAttr.setWritable(0)
 
    RayCast.addAttribute(RayCast.GROUNDMESH)
    RayCast.addAttribute(RayCast.OBJECTMESH)
    RayCast.addAttribute(RayCast.TIME)
    RayCast.addAttribute(RayCast.RAYSOURCE)
    RayCast.addAttribute(RayCast.OBJECTPOS)
    RayCast.addAttribute(RayCast.MAXSEARCHDISTANCE)
    RayCast.addAttribute(RayCast.ENVELOPE)
 
    RayCast.addAttribute(RayCast.OUTPOSITION)
 
    RayCast.attributeAffects(RayCast.GROUNDMESH, RayCast.OUTPOSITION)
    RayCast.attributeAffects(RayCast.OBJECTMESH, RayCast.OUTPOSITION)
    RayCast.attributeAffects(RayCast.TIME, RayCast.OUTPOSITION)
    RayCast.attributeAffects(RayCast.RAYSOURCE, RayCast.OUTPOSITION)
    RayCast.attributeAffects(RayCast.ENVELOPE, RayCast.OUTPOSITION)
    RayCast.attributeAffects(RayCast.MAXSEARCHDISTANCE, RayCast.OUTPOSITION)
 
 
def initializePlugin(mobject):
    mplugin = omMPx.MFnPlugin(mobject)
    try:
        mplugin.registerNode(nodeName, nodeId, nodeCreator, nodeInit)
    except:
        sys.stderr.write("Error loading")
        raise
 
 
def uninitializePlugin(mobject):
    mplugin = omMPx.MFnPlugin(mobject)
    try:
        mplugin.deregisterNode(nodeId)
    except:
        sys.stderr.write("Error removing")
        raise