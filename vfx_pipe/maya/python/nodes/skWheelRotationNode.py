import sys
import maya.OpenMaya as om
import maya.OpenMayaMPx as omMPx
import pymel.core as pm
import pymel.core.datatypes as dt
import math
import maya.mel as mel

nodeName = "skWheelRotation"
nodeId = om.MTypeId(0x919190)


class wheelRot(omMPx.MPxNode):
    WHEELRAD = om.MObject()
    WHEELPOS = om.MObject()
    WHEELROTOUT = om.MObject()
    REFERENCEPOS = om.MObject()
    ROTPERSEC = om.MObject()
    TIMEIN = om.MObject()
    ENVELOPE = om.MObject()

    def __init__(self):
        omMPx.MPxNode.__init__(self)
        self.referencePosOld = dt.VectorN(0,0,0)
        self.rotateOld = 0.0

    def compute(self, plug, dataBlock):
        if plug == wheelRot.WHEELROTOUT:
            wheelRadius = dataBlock.inputValue(wheelRot.WHEELRAD)
            rotPerSec = dataBlock.inputValue(wheelRot.ROTPERSEC)
            timeIn = dataBlock.inputValue(wheelRot.TIMEIN)
            envelope = dataBlock.inputValue(wheelRot.ENVELOPE)
            referencePosMatrixHandle = dataBlock.inputValue(wheelRot.REFERENCEPOS)
            referencePosMatrix = om.MTransformationMatrix(referencePosMatrixHandle.asMatrix())
            wheelPosMatrixHandle = dataBlock.inputValue(wheelRot.WHEELPOS)
            wheelPosMatrix = om.MTransformationMatrix(wheelPosMatrixHandle.asMatrix())
            outRotHandle = dataBlock.outputValue(wheelRot.WHEELROTOUT)
    
            wheelPos = dt.VectorN(wheelPosMatrix.getTranslation(4))
            referencePos = dt.VectorN(referencePosMatrix.getTranslation(4))
            oldVector = self.referencePosOld
            oldRot = self.rotateOld
			
            wheelRadiusHandle = wheelRadius.asFloat()
            rotPerSecHandle = rotPerSec.asFloat()
            timeInHandle = timeIn.asFloat()
            envelopeHandle = envelope.asFloat()
			
            if envelopeHandle > 1.0:
                envelopeHandle = 1.0
            if envelopeHandle < 0.0:
                envelopeHandle = 0.0
			
            fps = mel.eval('currentTimeUnitToFPS')
            manualRotation = ((rotPerSecHandle * 360)/fps)*timeInHandle
            
            wheelVector = dt.VectorN(referencePos - wheelPos)
            referenceVector = dt.VectorN(referencePos - oldVector)
            
            distance = referenceVector.length()
            
            wheelVector.normalize()
            referenceVector.normalize()
            
            dot = dt.VectorN.dot(referenceVector, wheelVector)
            
            wheelRotation = (oldRot + 360 / (2* math.pi * wheelRadiusHandle) * (dot * distance) * envelopeHandle)
            
            outRotHandle.setFloat((wheelRotation+manualRotation))
			
            self.rotateOld = wheelRotation
            self.referencePosOld = referencePos


def nodeCreator():
    return omMPx.asMPxPtr(wheelRot())


def nodeInit():
    mAttr = om.MFnMatrixAttribute()
    wheelRot.WHEELPOS = mAttr.create("wheelMatrix", "wMx")
    mAttr.setStorable(1)
    mAttr.setKeyable(1)

    numAttr = om.MFnNumericAttribute()
    wheelRot.WHEELRAD = numAttr.create("wheelRadius", "wRad", om.MFnNumericData.kFloat, 35.0)
    numAttr.setStorable(1)
    numAttr.setKeyable(1)
	
    numAttr = om.MFnNumericAttribute()
    wheelRot.ROTPERSEC = numAttr.create("rotPerSec", "rPS", om.MFnNumericData.kFloat, 0.0)
    numAttr.setStorable(1)
    numAttr.setKeyable(1)
	
    numAttr = om.MFnNumericAttribute()
    wheelRot.TIMEIN = numAttr.create("timeIn", "t", om.MFnNumericData.kFloat, 0.0)
    numAttr.setStorable(1)
    numAttr.setKeyable(1)
	
    numAttr = om.MFnNumericAttribute()
    wheelRot.ENVELOPE = numAttr.create("envelope", "env", om.MFnNumericData.kFloat, 1.0)
    numAttr.setStorable(1)
    numAttr.setKeyable(1)

    mAttr = om.MFnMatrixAttribute()
    wheelRot.REFERENCEPOS = mAttr.create("referenceMatrix", "refMx")
    mAttr.setStorable(1)
    mAttr.setKeyable(1)
    
    numAttr = om.MFnNumericAttribute()
    wheelRot.WHEELROTOUT = numAttr.create("wheelRotationOUT", "outRot", om.MFnNumericData.kFloat, 0.0)
    numAttr.setStorable(1)
    numAttr.setWritable(0)
    
    wheelRot.addAttribute(wheelRot.WHEELRAD)
    wheelRot.addAttribute(wheelRot.ROTPERSEC)
    wheelRot.addAttribute(wheelRot.TIMEIN)
    wheelRot.addAttribute(wheelRot.ENVELOPE)
    wheelRot.addAttribute(wheelRot.REFERENCEPOS)
    wheelRot.addAttribute(wheelRot.WHEELPOS)
    
    wheelRot.addAttribute(wheelRot.WHEELROTOUT)
    
    wheelRot.attributeAffects(wheelRot.WHEELRAD, wheelRot.WHEELROTOUT)
    wheelRot.attributeAffects(wheelRot.ROTPERSEC, wheelRot.WHEELROTOUT)
    wheelRot.attributeAffects(wheelRot.TIMEIN, wheelRot.WHEELROTOUT)
    wheelRot.attributeAffects(wheelRot.ENVELOPE, wheelRot.WHEELROTOUT)
    wheelRot.attributeAffects(wheelRot.WHEELPOS, wheelRot.WHEELROTOUT)
    wheelRot.attributeAffects(wheelRot.REFERENCEPOS, wheelRot.WHEELROTOUT)


def initializePlugin(mobject):
    mplugin = omMPx.MFnPlugin(mobject)
    try:
        mplugin.registerNode(nodeName, nodeId, nodeCreator, nodeInit)
    except:
        sys.stderr.write("Error loading")
        raise


def uninitializePlugin(mobject):
    mplugin = omMPx.MFnPlugin(mobject)
    try:
        mplugin.deregisterNode(nodeId)
    except:
        sys.stderr.write("Error removing")
        raise


