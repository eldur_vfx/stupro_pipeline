from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui
from shiboken2 import wrapInstance
import maya.OpenMayaUI as omui
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
import os
import pymel.core as pm
import re
import threading

def maya_main_window():
    main_window_ptr = omui.MQtUtil.mainWindow()
    return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)

class skRenderChecker(MayaQWidgetDockableMixin, QtWidgets.QDialog):

    def __init__(self, parent=maya_main_window()):
        super(skRenderChecker, self).__init__(parent)

        self.setWindowTitle("skRenderChecker")
        self.setMinimumWidth(450)
        self.setMinimumHeight(400)

        # Remove the ? from the dialog on Windows
        self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowContextHelpButtonHint)

        self.create_widgets()
        self.create_layouts()
        self.connect_widgets()

    def create_widgets(self):
        
        self.directory_label = QtWidgets.QLabel("Directory:")
        self.directory_lineedit = QtWidgets.QLineEdit()
        self.directory_lineedit.setPlaceholderText("Path to directory.")
        self.directory_button = QtWidgets.QPushButton()
        self.directory_button.setIcon(QtGui.QIcon(":fileOpen.png"))
        self.directory_file_dialog = QtWidgets.QFileDialog()
        
        self.start_frame_label = QtWidgets.QLabel("Start:")
        self.start_frame_lineedit = QtWidgets.QLineEdit()
        self.start_frame_lineedit.setText(str(int(pm.playbackOptions(q=1, min=1))))
        self.start_frame_lineedit.setValidator(QtGui.QDoubleValidator())
        self.end_frame_label = QtWidgets.QLabel("End:")
        self.end_frame_lineedit = QtWidgets.QLineEdit()
        self.end_frame_lineedit.setText(str(int(pm.playbackOptions(q=1, max=1))))
        self.end_frame_lineedit.setValidator(QtGui.QDoubleValidator())
        self.threshhold_label = QtWidgets.QLabel("Threshhold(mb)")
        self.threshhold_lineedit = QtWidgets.QLineEdit()
        self.threshhold_lineedit.setText(str(0.0))
        self.threshhold_lineedit.setValidator(QtGui.QDoubleValidator())
        self.successfull_checkbox = QtWidgets.QCheckBox("Successfull")
        self.successfull_checkbox.setChecked(True)
        self.too_small_checkbox = QtWidgets.QCheckBox("Too Small")
        self.too_small_checkbox.setChecked(True)
        self.missing_checkbox = QtWidgets.QCheckBox("Missing")
        self.missing_checkbox.setChecked(True)

        self.pattern_label = QtWidgets.QLabel("Pattern:")
        self.pattern_lineedit = QtWidgets.QLineEdit()
        self.pattern_lineedit.setPlaceholderText("Replace frames with i.e. <#>")

        self.output_textedit = QtWidgets.QTextEdit()
        self.output_textedit.setReadOnly(True)
        self.search_button = QtWidgets.QPushButton("Search")
        self.close_button = QtWidgets.QPushButton("Close")

        self.setStyleSheet(r"QPushButton:hover{background: #278cbc; color: black} QGroupBox {font: bold 14px}")

    def create_layouts(self):
        
        search_params_box = QtWidgets.QGroupBox("Search Parameters")
        search_params_layout = QtWidgets.QGridLayout()
        search_params_layout.addWidget(self.directory_label, 0, 0)
        search_params_layout.addWidget(self.directory_lineedit, 0, 1, 1, 4)
        search_params_layout.addWidget(self.directory_button, 0, 5)
        search_params_layout.addWidget(self.start_frame_label, 1, 0)
        search_params_layout.addWidget(self.start_frame_lineedit, 1, 1)
        search_params_layout.addWidget(self.end_frame_label, 1, 2)
        search_params_layout.addWidget(self.end_frame_lineedit, 1, 3)
        search_params_layout.addWidget(self.threshhold_label, 1, 4)
        search_params_layout.addWidget(self.threshhold_lineedit, 1, 5)
        search_params_layout.addWidget(self.pattern_label, 2, 0)
        search_params_layout.addWidget(self.pattern_lineedit, 2, 1, 1, 4)
        search_params_layout.addWidget(self.successfull_checkbox, 3, 0, 1, 2)
        search_params_layout.addWidget(self.too_small_checkbox, 3, 2, 1, 2)
        search_params_layout.addWidget(self.missing_checkbox, 3, 4, 1, 2)
        search_params_box.setLayout(search_params_layout)

        result_box = QtWidgets.QGroupBox("Results")
        result_layout = QtWidgets.QVBoxLayout()
        result_layout.addWidget(self.output_textedit)
        result_box.setLayout(result_layout)

        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addStretch()
        button_layout.addWidget(self.search_button)
        button_layout.addWidget(self.close_button)

        main_layout = QtWidgets.QVBoxLayout(self)
        main_layout.addWidget(search_params_box)
        main_layout.addWidget(result_box)
        main_layout.addLayout(button_layout)


    def connect_widgets(self):
        self.search_button.clicked.connect(self.check_frames)
        self.close_button.clicked.connect(self.close)
        self.directory_button.clicked.connect(self.get_directory)
        self.successfull_checkbox.stateChanged.connect(self.check_frames)
        self.too_small_checkbox.stateChanged.connect(self.check_frames)
        self.missing_checkbox.stateChanged.connect(self.check_frames)

    def get_directory(self):
        directory = self.directory_file_dialog.getExistingDirectory()
        self.directory_lineedit.setText(directory)

    def check_frames(self):

        if self.directory_lineedit.text() != "":
            if self.pattern_lineedit.text() != "":

                self.output_textedit.clear()

                start_pattern, end_pattern, padding = self.get_pattern()

                ouput_frame_dict = self.get_errorneus_frames(start_pattern, end_pattern, padding)

                for key, value in sorted(ouput_frame_dict.items()):
                    if value["exists"]:
                        if value["correct_size"]:
                            out_text = "<span style=\" color:#42f495;\" >{}</span>".format(key)
                            if self.successfull_checkbox.isChecked():
                                self.output_textedit.append(out_text)
                        else:
                            out_text = "<span style=\" color:#f4ee42;\" >{}</span>".format(key)
                            if self.too_small_checkbox.isChecked():
                                self.output_textedit.append(out_text)
                    else:
                        out_text = "<span style=\" color:#f44141;\" >{}</span>".format(key)
                        if self.missing_checkbox.isChecked():
                            self.output_textedit.append(out_text)
            else:
                self.missing_field_warning(self.pattern_lineedit)
        else:
            if self.pattern_lineedit.text() == "":
                self.missing_field_warning(self.pattern_lineedit)
            self.missing_field_warning(self.directory_lineedit)

    def get_pattern(self):

        text = self.pattern_lineedit.text()

        if text != "":
            padding = text.count("#")

            pattern = re.compile("<#+>")
            res = re.findall(pattern, text)
            if len(res) != 0:
                text = text.split(res[0])
                return text[0], text[1], padding

    def get_errorneus_frames(self, start_pattern, end_pattern, padding):
        file_dir = self.directory_lineedit.text()

        pattern_length = len(start_pattern) + len(end_pattern) + padding

        if os.path.isdir(file_dir):

            render_files = {}

            for root, dirs, files in os.walk(file_dir):
                if len(files) == 0:
                    continue
                render_files[root] = []
                for file in files:
                    render_files[root].append(file)

            threshhold = float(self.threshhold_lineedit.text()) * 1000000

            start_frame = int(float(self.start_frame_lineedit.text()))
            end_frame = int(float(self.end_frame_lineedit.text()))

            output_frame_dict = {}

            for render_dir in render_files:
                for frame_number in range(start_frame, end_frame+1):   
                    if padding != 0:
                        frame_nr = str("%0{}d".format(padding) % frame_number)
                        search_frame = "".join([start_pattern, frame_nr, end_pattern])
                    else:
                        continue

                    matching = [s for s in render_files[render_dir] if search_frame in s]

                    if len(matching) != 0:
                        matching = matching[0]

                    if len(matching) != 0:
                        size = os.path.getsize(os.path.join(render_dir, matching))
                        if size < threshhold:
                            output_frame_dict[search_frame] = {
                                "exists" : True,
                                "correct_size" : False,
                                "size" : size / 1000000
                            }
                        else:
                            output_frame_dict[search_frame] = {
                                "exists" : True,
                                "correct_size" : True,
                                "size" : size / 1000000
                            }
                    else:
                        if search_frame in output_frame_dict.keys():
                            if output_frame_dict[search_frame]["exists"]:
                                continue
                        else:
                            output_frame_dict[search_frame] = {
                                    "exists" : False,
                                    "correct_size" : None,
                                    "size" : None
                                }

            return output_frame_dict

        else:
            self.missing_field_warning(self.directory_lineedit)
            return({})

    def missing_field_warning(self, *args):
        for arg in args:
            arg.setStyleSheet("border: 1px solid #f4424b; border-radius: 1px")
            threading.Timer(2, lambda: self.clear_warning(arg)).start()

    def clear_warning(self, layout):
        layout.setStyleSheet("")


    def set_search_dir(self, search_dir):
        self.directory_lineedit.setText(search_dir)

def main():
    try:
        d.close() # pylint: disable=E0601
        d.deleteLater()
    except:
        pass
    d = skRenderChecker()
    d.show(dockable=True)