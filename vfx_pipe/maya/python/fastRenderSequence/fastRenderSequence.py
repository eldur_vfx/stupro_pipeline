import maya.cmds as cmds
import maya.mel as mel
import os

def showRenderer():

    #open Render otherwise Problems with settings
    mel.eval('unifiedRenderGlobalsWindow;');

    #change Render Settings
    cmds.setAttr("defaultResolution.aspectLock", 1);
    cmds.setAttr("defaultArnoldDriver.mergeAOVs", 1);
    mel.eval('setMayaSoftwareFrameExt("3", 0);');
    cmds.setAttr ("perspShape.depth", 1);
    cmds.setAttr("perspShape.mask", 1);

    #Aspect Ratio Sigma 2083 1171 CP3: '2085x1173'
    newWidth = 2103
    newHeight = 1183
    cmds.setAttr("defaultResolution.width", newWidth);
    cmds.setAttr("defaultResolution.height", newHeight);
    cmds.setAttr("defaultResolution.deviceAspectRatio", (newWidth / newHeight));
    cmds.setAttr("defaultResolution.lockDeviceAspectRa tio", 0);
    cmds.setAttr("defaultResolution.pixelAspect", 1.0);

    #frame Range

    frame_from = cmds.playbackOptions(q=True, animationStartTime=True)
    frame_to = cmds.playbackOptions(q=True, animationEndTime=True)

    cmds.setAttr("defaultRenderGlobals.startFrame", frame_from);
    cmds.setAttr("defaultRenderGlobals.endFrame", frame_to);

    # Set Directory

    mel.eval('string $text = "'+getRenderDir()+'";');
    mel.eval('optionVar - stringValue "RenderSequenceAlternateOutputFileLocation" $text;');

    mel.eval("renderSequenceOptionsWindow;");

    print("Start with Rendering :)")


def getRenderDir():
    workfolder = mel.eval("workspace -q -rd;");
    if workfolder:
        dir_list = workfolder.split('/');

        if dir_list[4] in ["_ASSETS"]:
            dir_list = dir_list[:-4];
            root_dir = '/'.join(dir_list) + '/';
            scene_name = cmds.file(q=True, shortName=True, sceneName=True);
            file_name = scene_name.split(".")[-2]
            export_folder = os.path.join(root_dir + '_export/img-fin/' + file_name + '/');
            return export_folder

        elif dir_list[4] in ["_SHOTS"]:
            dir_list = dir_list[:-3];
            root_dir = '/'.join(dir_list) + '/';
            scene_name = cmds.file(q=True, shortName=True, sceneName=True);
            file_name = scene_name.split(".")[-2]
            export_folder = os.path.join(root_dir + '_export/img-fin/' + file_name + '/');
            return export_folder

    return None


