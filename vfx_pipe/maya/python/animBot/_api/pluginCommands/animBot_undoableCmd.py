




import sys
import _ctypes

import maya.OpenMayaMPx as OpenMayaMPx


o_____l__l____O_____o__O____o___0___1___1__0_l_____o____1___l___O_o_0___O___l____l_o = "animBot_undoableCmd"

try:
    long
except NameError:
    
    long = int


class o___l_O_O____o____1__o_____1____l_o_0_____l____1__1(OpenMayaMPx.MPxCommand):

    def isUndoable(o__1___0_____o__o_1):
        
        return True

    def doIt(o__1___0_____o__o_1, args):
        



        o____0_O____l___O___O_o____l__1____o___O = long(args.asString(0), 0)
        o__1___0_____o__o_1.o____o___o_____0____o___o__0___0_____1___1__o_____l = _ctypes.PyObj_FromPtr(o____0_O____l___O___O_o____l__1____o___O)

        o_____1_o___l____o__0_____o_1 = long(args.asString(1), 0)
        o__1___0_____o__o_1.o____1____0____O____0_____o___O___O = _ctypes.PyObj_FromPtr(o_____1_o___l____o__0_____o_1)

        if o__1___0_____o__o_1.o____1____0____O____0_____o___O___O is not None:
            o__1___0_____o__o_1.o____1____0____O____0_____o___O___O.doIt()

    def redoIt(o__1___0_____o__o_1):
        if o__1___0_____o__o_1.o____o___o_____0____o___o__0___0_____1___1__o_____l is not None:
            o__1___0_____o__o_1.o____o___o_____0____o___o__0___0_____1___1__o_____l.redoIt()

        if o__1___0_____o__o_1.o____1____0____O____0_____o___O___O is not None:
            o__1___0_____o__o_1.o____1____0____O____0_____o___O___O.doIt()

    def undoIt(o__1___0_____o__o_1):
        if o__1___0_____o__o_1.o____o___o_____0____o___o__0___0_____1___1__o_____l is not None:
            o__1___0_____o__o_1.o____o___o_____0____o___o__0___0_____1___1__o_____l.undoIt()

        if o__1___0_____o__o_1.o____1____0____O____0_____o___O___O is not None:
            o__1___0_____o__o_1.o____1____0____O____0_____o___O___O.undoIt()




def cmdCreator():
    
    return OpenMayaMPx.asMPxPtr(o___l_O_O____o____1__o_____1____l_o_0_____l____1__1())


def initializePlugin(o____O___0_1_____0__0):
    
    plugin = OpenMayaMPx.MFnPlugin(o____O___0_1_____0__0, "animBot", "1.0", "Any")
    try:
        plugin.registerCommand(o_____l__l____O_____o__O____o___0___1___1__0_l_____o____1___l___O_o_0___O___l____l_o, cmdCreator)
    except:
        sys.stderr.write("Failed to register command: " + o_____l__l____O_____o__O____o___0___1___1__0_l_____o____1___l___O_o_0___O___l____l_o)


def uninitializePlugin(o____O___0_1_____0__0):
    
    plugin = OpenMayaMPx.MFnPlugin(o____O___0_1_____0__0)
    try:
        plugin.deregisterCommand(o_____l__l____O_____o__O____o___0___1___1__0_l_____o____1___l___O_o_0___O___l____l_o)
    except:
        sys.stderr.write("Failed to unregister command: " + o_____l__l____O_____o__O____o___0___1___1__0_l_____o____1___l___O_o_0___O___l____l_o)


