from animBot._api.core import *
from animBot.motionTrail.motionTrailDrawing import MotionTrailDrawing

import sys
from maya.OpenMayaMPx import *


o____0_1__o__1_l_____O_o_____l___0__o___o__0_____O___1_____1____1__O_o___1____o____O_____1 = "animBotMotionTrail"


if o_____l____O___l_o___1__O____0___0_0____O_o__l_____l_1_O___O___o____1_____l__l___O_1____O >= 2016:
    MPxDrawOverride = OpenMayaRenderApi.MPxDrawOverride
else:
    MPxDrawOverride = object






class MotionTrail(MPxLocatorNode):
    
    id = MTypeId(0x0012d040)
    o___o___o__1_0_0____1____O_o__1___l_____l_____1____o_____1 = "drawdb/geometry/animBotMotionTrail"
    o___o__0_0_O_____l__0__l_l__o__O__O____l_l_0__O = "animBotMotionTrailNodePlugin"


    def __init__(o__1___0_____o__o_1):
        MPxLocatorNode.__init__(o__1___0_____o__o_1)
        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o = None

    @staticmethod
    def creator():
        return MotionTrail()

    @staticmethod
    def initialize():
        o_O_o_____0_____l_0___l_o_O___o_____o_____O = MFnMessageAttribute()
        o____1___1_____1___o__O____1____0___l_o_0_____1 = MFnTypedAttribute()
        o__0_O_0____1__O_____o_0_____O = MFnNumericAttribute()
        o_l___1__o__l_____o__l__O____1_O = MFnUnitAttribute()
        o_____1____l_O_____0_0____O__0_____1__l_____1_l = MFnMatrixAttribute()
        o___0_l___1__O___1___1____O_____0____l = MFnEnumAttribute()

        
        
        MotionTrail.CAMERA_NODE_MESSAGE = o_O_o_____0_____l_0___l_o_O___o_____o_____O.create("cameraNodeMessage", "cameraNodeMessage")
        o_O_o_____0_____l_0___l_o_O___o_____o_____O.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.CAMERA_NODE_MESSAGE)

        
        MotionTrail.CAMERA_SPACE_MESSAGE = o_O_o_____0_____l_0___l_o_O___o_____o_____O.create("cameraSpaceMessage", "cameraSpaceMessage")
        o_O_o_____0_____l_0___l_o_O___o_____o_____O.setArray(True)
        o_O_o_____0_____l_0___l_o_O___o_____o_____O.setIndexMatters(False)
        o_O_o_____0_____l_0___l_o_O___o_____o_____O.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.CAMERA_SPACE_MESSAGE)

        
        MotionTrail.CAMERA_SPACE_MATRIX = o_____1____l_O_____0_0____O__0_____1__l_____1_l.create("cameraSpaceMatrix", "cameraSpaceMatrix", MFnMatrixAttribute.kDouble)
        o_____1____l_O_____0_0____O__0_____1__l_____1_l.setArray(True)
        o_____1____l_O_____0_0____O__0_____1__l_____1_l.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.CAMERA_SPACE_MATRIX)

        
        MotionTrail.OBJECT_SPACE_MESSAGE = o_O_o_____0_____l_0___l_o_O___o_____o_____O.create("objectSpaceMessage", "objectSpaceMessage")
        o_O_o_____0_____l_0___l_o_O___o_____o_____O.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.OBJECT_SPACE_MESSAGE)

        
        MotionTrail.WORLD_SPACE_POINTS = o____1___1_____1___o__O____1____0___l_o_0_____1.create("worldSpacePoints", "worldSpacePoints", MFnData.kPointArray)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.WORLD_SPACE_POINTS)

        
        MotionTrail.ORIENTATION_WORLD_SPACE_POINTS_X = o____1___1_____1___o__O____1____0___l_o_0_____1.create("orientationWorldSpacePointsX", "orientationWorldSpacePointsX", MFnData.kPointArray)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.ORIENTATION_WORLD_SPACE_POINTS_X)
        MotionTrail.ORIENTATION_WORLD_SPACE_POINTS_Y = o____1___1_____1___o__O____1____0___l_o_0_____1.create("orientationWorldSpacePointsY", "orientationWorldSpacePointsY", MFnData.kPointArray)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.ORIENTATION_WORLD_SPACE_POINTS_Y)
        MotionTrail.ORIENTATION_WORLD_SPACE_POINTS_Z = o____1___1_____1___o__O____1____0___l_o_0_____1.create("orientationWorldSpacePointsZ", "orientationWorldSpacePointsZ", MFnData.kPointArray)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.ORIENTATION_WORLD_SPACE_POINTS_Z)

        
        MotionTrail.OBJECT_SPACE_MATRIX = o_____1____l_O_____0_0____O__0_____1__l_____1_l.create("objectSpaceMatrix", "objectSpaceMatrix", MFnMatrixAttribute.kDouble)
        o_____1____l_O_____0_0____O__0_____1__l_____1_l.setArray(True)
        o_____1____l_O_____0_0____O__0_____1__l_____1_l.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.OBJECT_SPACE_MATRIX)

        
        MotionTrail.DIRTY_DOTS = o____1___1_____1___o__O____1____0___l_o_0_____1.create("dirtyDots", "dirtyDots", MFnData.kIntArray)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.DIRTY_DOTS)

        
        MotionTrail.FRAME_TIMES = o____1___1_____1___o__O____1____0___l_o_0_____1.create("frameTimes", "frameTimes", MFnData.kDoubleArray)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.FRAME_TIMES)

        
        MotionTrail.GIMBAL_LOCK_SCORE = o____1___1_____1___o__O____1____0___l_o_0_____1.create("gimbalLockScore", "gimbalLockScore", MFnData.kDoubleArray)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.GIMBAL_LOCK_SCORE)

        
        MotionTrail.BIG_DOTS = o____1___1_____1___o__O____1____0___l_o_0_____1.create("bigDots", "bigDots", MFnData.kIntArray)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.BIG_DOTS)

        
        MotionTrail.LINE_WIDTH = o__0_O_0____1__O_____o_0_____O.create("lineWidth", "lineWidth", MFnNumericData.kInt, 1)
        o__0_O_0____1__O_____o_0_____O.setMin(0)
        o__0_O_0____1__O_____o_0_____O.setMax(20)
        MotionTrail.addAttribute(MotionTrail.LINE_WIDTH)

        
        MotionTrail.SMALL_DOT_SIZE = o__0_O_0____1__O_____o_0_____O.create("smallDotSize", "smallDotSize", MFnNumericData.kInt, 1)
        o__0_O_0____1__O_____o_0_____O.setMin(0)
        o__0_O_0____1__O_____o_0_____O.setMax(20)
        MotionTrail.addAttribute(MotionTrail.SMALL_DOT_SIZE)

        
        MotionTrail.BIG_DOT_SIZE = o__0_O_0____1__O_____o_0_____O.create("bigDotSize", "bigDotSize", MFnNumericData.kInt, 1)
        o__0_O_0____1__O_____o_0_____O.setMin(0)
        o__0_O_0____1__O_____o_0_____O.setMax(20)
        MotionTrail.addAttribute(MotionTrail.BIG_DOT_SIZE)

        
        MotionTrail.PRESENT_SMALL_DOT_COLOR = o__0_O_0____1__O_____o_0_____O.createColor("presentSmallDotColor", "presentSmallDotColor")
        MotionTrail.addAttribute(MotionTrail.PRESENT_SMALL_DOT_COLOR)

        
        MotionTrail.PRESENT_BIG_DOT_COLOR = o__0_O_0____1__O_____o_0_____O.createColor("presentBigDotColor", "presentBigDotColor")
        MotionTrail.addAttribute(MotionTrail.PRESENT_BIG_DOT_COLOR)

        
        MotionTrail.PAST_DOTS_COLOR = o__0_O_0____1__O_____o_0_____O.createColor("pastDotsColor", "pastDotsColor")
        MotionTrail.addAttribute(MotionTrail.PAST_DOTS_COLOR)

        
        MotionTrail.PAST_DIRTY_DOTS_COLOR = o__0_O_0____1__O_____o_0_____O.createColor("pastDirtyDotsColor", "pastDirtyDotsColor")
        MotionTrail.addAttribute(MotionTrail.PAST_DIRTY_DOTS_COLOR)

        
        MotionTrail.FUTURE_DOTS_COLOR = o__0_O_0____1__O_____o_0_____O.createColor("futureDotsColor", "futureDotsColor")
        MotionTrail.addAttribute(MotionTrail.FUTURE_DOTS_COLOR)

        
        MotionTrail.FUTURE_DIRTY_DOTS_COLOR = o__0_O_0____1__O_____o_0_____O.createColor("futureDirtyDotsColor", "futureDirtyDotsColor")
        MotionTrail.addAttribute(MotionTrail.FUTURE_DIRTY_DOTS_COLOR)

        
        MotionTrail.o_O____o___0___0___o__O___O__o_1___1___o = 0
        MotionTrail.o___0_____l____O_____0___o_l__O___l__o_____1____O___o__0 = 1
        MotionTrail.o__1___1____O__l___0 = 2
        MotionTrail.o___o_____0_1__o__O__0__O____1_____o = 3
        MotionTrail.o_O_____o___o__l__0_____O_0___0__o = 4

        MotionTrail.LINE_COLOR_SCHEME = o___0_l___1__O___1___1____O_____0____l.create("lineColorScheme", "lineColorScheme")
        o___0_l___1__O___1___1____O_____0____l.addField("Past and Future", MotionTrail.o_O____o___0___0___o__O___O__o_1___1___o)
        o___0_l___1__O___1___1____O_____0____l.addField("Spacing Gradient", MotionTrail.o___0_____l____O_____0___o_l__O___l__o_____1____O___o__0)
        o___0_l___1__O___1___1____O_____0____l.addField("Gimbal Lock Gradient", MotionTrail.o__1___1____O__l___0)
        o___0_l___1__O___1___1____O_____0____l.addField("Noise Gradient", MotionTrail.o___o_____0_1__o__O__0__O____1_____o)
        o___0_l___1__O___1___1____O_____0____l.addField("Depth Gradient", MotionTrail.o_O_____o___o__l__0_____O_0___0__o)
        MotionTrail.addAttribute(MotionTrail.LINE_COLOR_SCHEME)

        
        MotionTrail.LINE_COLOR_1 = o__0_O_0____1__O_____o_0_____O.createColor("lineColor1", "lineColor1")
        MotionTrail.addAttribute(MotionTrail.LINE_COLOR_1)

        
        MotionTrail.LINE_COLOR_2 = o__0_O_0____1__O_____o_0_____O.createColor("lineColor2", "lineColor2")
        MotionTrail.addAttribute(MotionTrail.LINE_COLOR_2)

        
        MotionTrail.LINE_COLOR_3 = o__0_O_0____1__O_____o_0_____O.createColor("lineColor3", "lineColor3")
        MotionTrail.addAttribute(MotionTrail.LINE_COLOR_3)

        
        MotionTrail.LINE_COLOR_4 = o__0_O_0____1__O_____o_0_____O.createColor("lineColor4", "lineColor4")
        MotionTrail.addAttribute(MotionTrail.LINE_COLOR_4)

        
        MotionTrail.FRET_LINE_COLOR_X_1 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorX1", "fretLineColorX1")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_X_1)

        
        MotionTrail.FRET_LINE_COLOR_X_2 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorX2", "fretLineColorX2")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_X_2)

        
        MotionTrail.FRET_LINE_COLOR_X_3 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorX3", "fretLineColorX3")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_X_3)

        
        MotionTrail.FRET_LINE_COLOR_Y_1 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorY1", "fretLineColorY1")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_Y_1)

        
        MotionTrail.FRET_LINE_COLOR_Y_2 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorY2", "fretLineColorY2")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_Y_2)

        
        MotionTrail.FRET_LINE_COLOR_Y_3 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorY3", "fretLineColorY3")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_Y_3)

        
        MotionTrail.FRET_LINE_COLOR_Z_1 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorZ1", "fretLineColorZ1")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_Z_1)

        
        MotionTrail.FRET_LINE_COLOR_Z_2 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorZ2", "fretLineColorZ2")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_Z_2)

        
        MotionTrail.FRET_LINE_COLOR_Z_3 = o__0_O_0____1__O_____o_0_____O.createColor("fretLineColorZ3", "fretLineColorZ3")
        MotionTrail.addAttribute(MotionTrail.FRET_LINE_COLOR_Z_3)

        
        MotionTrail.TIME = o_l___1__o__l_____o__l__O____1_O.create("time", "time", MFnUnitAttribute.kTime, 0.0)
        MotionTrail.addAttribute(MotionTrail.TIME)

        
        MotionTrail.o___0__0___1____l__0___O_o_0_____o_o__l__1_o__l____o__O____o_____O_____O____0____1_____O__1_0_____0 = o_l___1__o__l_____o__l__O____1_O.create("framesBeforeAfter", "framesBeforeAfter", MFnUnitAttribute.kTime, 0.0)
        o_l___1__o__l_____o__l__O____1_O.setMin(0)
        MotionTrail.addAttribute(MotionTrail.o___0__0___1____l__0___O_o_0_____o_o__l__1_o__l____o__O____o_____O_____O____0____1_____O__1_0_____0)

        
        MotionTrail.ORIENTATION_TRACKING_X = o__0_O_0____1__O_____o_0_____O.create("orientationTrackingX", "orientationTrackingX", MFnNumericData.kBoolean, 0)
        MotionTrail.addAttribute(MotionTrail.ORIENTATION_TRACKING_X)
        MotionTrail.ORIENTATION_TRACKING_Y = o__0_O_0____1__O_____o_0_____O.create("orientationTrackingY", "orientationTrackingY", MFnNumericData.kBoolean, 0)
        MotionTrail.addAttribute(MotionTrail.ORIENTATION_TRACKING_Y)
        MotionTrail.ORIENTATION_TRACKING_Z = o__0_O_0____1__O_____o_0_____O.create("orientationTrackingZ", "orientationTrackingZ", MFnNumericData.kBoolean, 0)
        MotionTrail.addAttribute(MotionTrail.ORIENTATION_TRACKING_Z)

        
        MotionTrail.TRANSPARENCY_FALLOFF = o__0_O_0____1__O_____o_0_____O.create("transparencyFalloff", "transparencyFalloff", MFnNumericData.kBoolean, 0)
        MotionTrail.addAttribute(MotionTrail.TRANSPARENCY_FALLOFF)

        
        MotionTrail.IS_DIRTY = o__0_O_0____1__O_____o_0_____O.create("isDirty", "isDirty", MFnNumericData.kBoolean, 1)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.IS_DIRTY)

        
        MotionTrail.IS_CAMERA_DIRTY = o__0_O_0____1__O_____o_0_____O.create("isCameraDirty", "isCameraDirty", MFnNumericData.kBoolean, 1)
        o____1___1_____1___o__O____1____0___l_o_0_____1.setHidden(True)
        MotionTrail.addAttribute(MotionTrail.IS_CAMERA_DIRTY)


    def compute(o__1___0_____o__o_1, plug, o__o__l_1__0___o____1__1_O____0__1__1_1___O):
        return None

    def excludeAsLocator(o__1___0_____o__o_1):
        
        return False

    def drawLast(o__1___0_____o__o_1):
        
        return True

    def isBounded(o__1___0_____o__o_1):
        
        return False

    def draw(o__1___0_____o__o_1, view, path, style, status):
        

        
        if not o__1___0_____o__o_1.o_____o__0_O___o____1_____1(view):
            return

        
        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o = o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o or MotionTrailDrawing(o__1___0_____o__o_1.thisMObject(), MotionTrail)

        
        o___l____l____0__o____o__O_____0_O = MDagPath()
        view.getCamera(o___l____l____0__o____o__O_____0_O)

        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o.o_O__l___0_____1_l(o___l____l____0__o____o__O_____0_O)
        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.setView(view)
        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.o___o_____l_____o_0____1____1__l____1(o_____o_o___o__0_____o_____1_l_0_1____0___O__0.kLegacy)
        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.o__O____O_____l_o___1____O_l__o__o____0_____O____O(o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o.o____o_____O__l___O_____0____l__O___0____O_l__0____1_1())
        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.draw()

    def o_____o__0_O___o____1_____1(o__1___0_____o__o_1, view):
        
        try:
            return OpenMayaUI.M3dView.kExcludeMotionTrails == (OpenMayaUI.M3dView.kExcludeMotionTrails & view.objectDisplay())

        except AttributeError:
            
            if o_____l____O___l_o___1__O____0___0_0____O_o__l_____l_1_O___O___o____1_____l__l___O_1____O >= 2017:
                raise

            
            return True

    
    
    
    
    
    
    
    
    












class o____0_____o__l___O_____o__o___1____1__1__l(OpenMayaApi.MUserData):
    def __init__(o__1___0_____o__o_1, o__l_____1____0__O___0____o_____1___0_o):
        OpenMayaApi.MUserData.__init__(o__1___0_____o__o_1, False)  

        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o = o__l_____1____0__O___0____o_____1___0_o



class o_____o___O___1_1___0_____o_____o___0_O(MPxDrawOverride):

    def __init__(o__1___0_____o__o_1, o_____0___0___l_1__0__0):
        OpenMayaRenderApi.MPxDrawOverride.__init__(o__1___0_____o__o_1, o_____0___0___l_1__0__0, o_____o___O___1_1___0_____o_____o___0_O.draw)

        o__1___0_____o__o_1.o__l_____1____0__O___0____o_____1___0_o = None

    @staticmethod
    def creator(o_____0___0___l_1__0__0):
        return o_____o___O___1_1___0_____o_____o___0_O(o_____0___0___l_1__0__0)

    @staticmethod
    def draw(context, data):
        return

    def isBounded(o__1___0_____o__o_1, o____l___l___o_l_____l, cameraPath):
        
        return False

    def supportedDrawAPIs(o__1___0_____o__o_1):
        
        return OpenMayaRenderApi.MRenderer.kOpenGL | OpenMayaRenderApi.MRenderer.kDirectX11 | OpenMayaRenderApi.MRenderer.kOpenGLCoreProfile

    def disableInternalBoundingBoxDraw(o__1___0_____o__o_1):
        return True

    def prepareForDraw(o__1___0_____o__o_1, o____l___l___o_l_____l, cameraPath, o___l_____1___O___1_o____1_0____1____O__l____l, o_l____O__l_l__l____0____0):
        

        
        if not o__1___0_____o__o_1.o_____o__0_O___o____1_____1(o___l_____1___O___1_o____1_0____1____O__l____l):
            return

        
        
        o____O___0_1_____0__0 = o_l___0_O_____0_O____o_____l__l(o____l___l___o_l_____l.fullPathName()).o____O___0_1_____0__0

        
        cameraPath = o_l___0_O_____0_O____o_____l__l(cameraPath.fullPathName()).o_____1___1___0_____o__O_0__0___O_l__1_____o____1

        
        o____o_____l_____o_____l____O__1__1 = o___l_____1___O___1_o____1_0____1____O__l____l.renderingDestination()[1]
        view = M3dView()
        M3dView.getM3dViewFromModelPanel(o____o_____l_____o_____l____O__1__1, view)

        
        o__l_____1____0__O___0____o_____1___0_o = MotionTrailDrawing(o____O___0_1_____0__0, MotionTrail)

        o__l_____1____0__O___0____o_____1___0_o.o_O__l___0_____1_l(cameraPath)
        o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.setView(view)
        o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.o___o_____l_____o_0____1____1__l____1(o_____o_o___o__0_____o_____1_l_0_1____0___O__0.o____l__0___1__o____1__l___l__1_O____O____0)
        o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.o__O____O_____l_o___1____O_l__o__o____0_____O____O(o__l_____1____0__O___0____o_____1___0_o.o____o_____O__l___O_____0____l__O___0____O_l__0____1_1())

        o_l____O__l_l__l____0____0 = o____0_____o__l___O_____o__o___1____1__1__l(o__l_____1____0__O___0____o_____1___0_o)

        return o_l____O__l_l__l____0____0



    def o_____o__0_O___o____1_____1(o__1___0_____o__o_1, o___l_____1___O___1_o____1_0____1____O__l____l):
        

        try:
            return OpenMayaRenderApi.MFrameContext.kExcludeMotionTrails != (OpenMayaRenderApi.MFrameContext.kExcludeMotionTrails & o___l_____1___O___1_o____1_0____1____O__l____l.objectTypeExclusions())

        except AttributeError:
            
            if o_____l____O___l_o___1__O____0___0_0____O_o__l_____l_1_O___O___o____1_____l__l___O_1____O >= 2017:
                raise

            o____o_____l_____o_____l____O__1__1 = o___l_____1___O___1_o____1_0____1____O__l____l.renderingDestination()[1]
            o_l____0__o_O_o_____o = o_____o_o___o__0_____o_____1_l_0_1____0___O__0()

            return o_l____0__o_O_o_____o.o__l_____0_O_1_____0(o____o_____l_____o_____l____O__1__1, "motionTrails")

    def hasUIDrawables(o__1___0_____o__o_1):
        return True

    def addUIDrawables(o__1___0_____o__o_1, o____l___l___o_l_____l, o____1_____l__1_o___0___1____o____o__1_____0____l_O, o___l_____1___O___1_o____1_0____1____O__l____l, o_l____O__l_l__l____0____0):
        

        
        if not o__1___0_____o__o_1.o_____o__0_O___o____1_____1(o___l_____1___O___1_o____1_0____1____O__l____l):
            return

        
        o_l____O__l_l__l____0____0.o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.o___0__o_O__l_____0_O_____o___O_____l_____0__l(o____1_____l__1_o___0___1____o____o__1_____0____l_O)
        o_l____O__l_l__l____0____0.o__l_____1____0__O___0____o_____1___0_o.o____O_1____1____O____0_____1____l__1____l_0_____l____l___l____l_l.draw()



def initializePlugin(o____O___0_1_____0__0):
    plugin = MFnPlugin(o____O___0_1_____0__0, "animBot", "1.0", "Any")

    try:
        plugin.registerNode(o____0_1__o__1_l_____O_o_____l___0__o___o__0_____O___1_____1____1__O_o___1____o____O_____1, MotionTrail.id, MotionTrail.creator, MotionTrail.initialize, MPxNode.kLocatorNode, MotionTrail.o___o___o__1_0_0____1____O_o__1___l_____l_____1____o_____1)
    except:
        sys.stderr.write("Failed to register node: %s" % o____0_1__o__1_l_____O_o_____l___0__o___o__0_____O___1_____1____1__O_o___1____o____O_____1)
        raise

    if o_____l____O___l_o___1__O____0___0_0____O_o__l_____l_1_O___O___o____1_____l__l___O_1____O >= 2016:
        try:
            OpenMayaRenderApi.MDrawRegistry.registerDrawOverrideCreator(MotionTrail.o___o___o__1_0_0____1____O_o__1___l_____l_____1____o_____1, MotionTrail.o___o__0_0_O_____l__0__l_l__o__O__O____l_l_0__O, o_____o___O___1_1___0_____o_____o___0_O.creator)
        except:
            sys.stderr.write("Failed to register override: %s" % o____0_1__o__1_l_____O_o_____l___0__o___o__0_____O___1_____1____1__O_o___1____o____O_____1)
            raise



def uninitializePlugin(o____O___0_1_____0__0):
    plugin = MFnPlugin(o____O___0_1_____0__0)

    try:
        plugin.deregisterNode(MotionTrail.id)
    except:
        sys.stderr.write("Failed to deregister node: %s" % o____0_1__o__1_l_____O_o_____l___0__o___o__0_____O___1_____1____1__O_o___1____o____O_____1)
        pass

    if o_____l____O___l_o___1__O____0___0_0____O_o__l_____l_1_O___O___o____1_____l__l___O_1____O >= 2016:
        try:
            OpenMayaRenderApi.MDrawRegistry.deregisterDrawOverrideCreator(MotionTrail.o___o___o__1_0_0____1____O_o__1___l_____l_____1____o_____1, MotionTrail.o___o__0_0_O_____l__0__l_l__o__O__O____l_l_0__O)
        except:
            sys.stderr.write("Failed to deregister override: %s" % o____0_1__o__1_l_____O_o_____l___0__o___o__0_____O___1_____1____1__O_o___1____o____O_____1)
            pass
