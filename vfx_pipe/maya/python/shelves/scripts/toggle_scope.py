import pymel.core as pm

def main():
    if pm.getAttr("defaultResolution.height") < 1000:    
        newWidth = 2048
        newHeight = 1152   
        rat = 1.778
    else:
        newWidth = 2048
        newHeight = 846
        rat = 2.421
        
    res = pm.PyNode("defaultResolution")
        
    res.aspectLock.set(0)   
    res.height.set(newHeight)
    res.width.set(newWidth)
    res.lockDeviceAspectRatio.set(0)
    res.deviceAspectRatio.set(rat)
    res.pixelAspect.set(1)
    res.aspectLock.set(1)