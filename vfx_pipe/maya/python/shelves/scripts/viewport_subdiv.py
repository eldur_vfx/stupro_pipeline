import pymel.core as pm

def main():
	trans = pm.ls(type="transform")
	 
	for item in trans:
	    if pm.nodeType(item.getShape()) == "mesh":
	        if item.hasAttr("Subdiv"):
	            if item.Subdiv.get() == 1:
	                pm.displaySmoothness(item, divisionsU=3, divisionsV=3, pointsWire=16, pointsShaded=4, polygonObject=3)
	            elif item.Subdiv.get() == 0:
	                pm.displaySmoothness(item, divisionsU=0, divisionsV=0, pointsWire=4, pointsShaded=1, polygonObject=1)