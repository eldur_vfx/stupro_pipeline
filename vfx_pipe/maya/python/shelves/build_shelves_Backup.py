# class customShelf(_shelf):
#     def build(self):
#         self.addButon(label="button1")
#         self.addButon("button2")
#         self.addButon("popup")
#         p = mc.popupMenu(b=1)
#         self.addMenuItem(p, "popupMenuItem1")
#         self.addMenuItem(p, "popupMenuItem2")
#         sub = self.addSubMenu(p, "subMenuLevel1")
#         self.addMenuItem(sub, "subMenuLevel1Item1")
#         sub2 = self.addSubMenu(sub, "subMenuLevel2")
#         self.addMenuItem(sub2, "subMenuLevel2Item1")
#         self.addMenuItem(sub2, "subMenuLevel2Item2")
#         self.addMenuItem(sub, "subMenuLevel1Item2")
#         self.addMenuItem(p, "popupMenuItem3")
#         self.addButon("button3")
# customShelf()
import shelfBase
reload(shelfBase)
import os
import getpass
import pymel.core as pm
import maya.cmds as cmds
import json

class StuProVFX_Shelf(shelfBase._shelf):
    def build(self):
    	this_dir = os.path.dirname(__file__).replace("\\", "/")
        self.addButton(label="Subdiv", icon=os.path.dirname(__file__)+"/icons/cake.png", command="import shelves.scripts.viewport_subdiv as vs; vs.main()", annotation="Set correct viewport subdivision depending on custom \"Subdiv\" attribute.", stp="python")
        self.addButton(label="JbRigIcons", command="import shelves.scripts.JbRigIcons.JbRigIcons as jb; jb.JbRigIcons().create()", stp="python")
        self.addButton(label="JbHashRename", command="import shelves.scripts.jbHashRename; reload(shelves.scripts.jbHashRename); shelves.scripts.jbHashRename.main()", stp="python")
        self.addButton(label="SubstanceToArnold", command="import shelves.scripts.Substance_to_Arnold_5; reload(shelves.scripts.Substance_to_Arnold_5); shelves.scripts.Substance_to_Arnold_5.main()", stp="python", annotation="Tool for importing substance maps to arnold shader.", icon=os.path.dirname(__file__)+"/icons/substance_logo.png")
        self.addButton(label="Toggle Scope", command="import shelves.scripts.toggle_scope; reload(shelves.scripts.toggle_scope); shelves.scripts.toggle_scope.main()", stp="python", annotation="Toggle between scope and 16:9.", icon=":/GateMask.png")
        self.addButton(label="Toggle Scope", command="import shelves.scripts.set_subdiv; reload(shelves.scripts.set_subdiv); shelves.scripts.set_subdiv.main()", stp="python", annotation="Set Arnold subdiv.", icon="")
        
        menu = self.addButton("UVTools", icon="UVtoolKit.png", annotation="Tools for mesh and uv actions.")
        p = cmds.popupMenu(parent=menu, b=1)
        with open(this_dir+"/scripts/uv_commands_json.json", "r") as json_in:
            commands_dict = json.load(json_in) 
        for key, value in commands_dict.items():
            self.addMenuItem(p, key, command=value["command"], icon=value["icon"], stp=value["source"], annotation=value["annotation"])
        self.addMenuItem(p, "Texel Density", command="import shelves.scripts.texelDensityAdvisor.texelDensityAdvisor as tda; tda.createUI", stp="python", annotation="Texel densitiy advisor ui.")
        self.addMenuItem(p, "UV Deluxe", command="import shelves.scripts.UV_Deluxe.UVDeluxe.uvdeluxe as uvd; uvd.createUI()", stp="python", annotation="UV Deluxe ui.")
        self.addMenuItem(p, "FindOverlapping", command="import shelves.scripts.findOverlappingUVs; reload(shelves.scripts.findOverlappingUVs); shelves.scripts.findOverlappingUVs.main()", stp="python", annotation="Find overlapping UVs in selected meshes or all meshes.")
        
class Dev_Shelf(shelfBase._shelf):
    def build(self):
        self.addButton(label="Reload", icon=":/mayaIcon.png", command="import setup.init_menu; reload(setup.init_menu); setup.init_menu.main()", annotation="Reload Tools menu.", stp="python")


def main():
    dev_list = ["Katha", "Denise", "dr069"]
    StuProVFX_Shelf(name="StuProVFX_Shelf")

    if getpass.getuser() in dev_list:
        Dev_Shelf(name="Dev")