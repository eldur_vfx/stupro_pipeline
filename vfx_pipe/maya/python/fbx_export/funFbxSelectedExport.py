import maya.cmds as cmds
import maya.mel as mel
import os
import pymel.core as pm

def showRenderer():
    pm.loadPlugin("fbxmaya")  # LOAD PLUGIN
    frame_from = cmds.playbackOptions(q=True, animationStartTime=True)
    frame_to = cmds.playbackOptions(q=True, animationEndTime=True)
    mel.eval("FBXExportBakeComplexStart -v " + str(frame_from))
    mel.eval("FBXExportBakeComplexEnd -v " + str(frame_to))
    renderDir = getExportDir()
    mel.eval("file - force - options \"v=0;\" - type \"FBX export\" - pr - es\"" + renderDir + "\";")


    print(getExportDir())
    print(getExportName())

def getExportDir():
    workfolder = mel.eval("workspace -q -rd;");
    if workfolder:
        dir_list = workfolder.split('/');

        if dir_list[4] in ["_ASSETS"]:
            dir_list = dir_list[:-4];
            root_dir = '/'.join(dir_list) + '/';
            scene_name = cmds.file(q=True, shortName=True, sceneName=True);
            file_name = scene_name.split(".")[-2]
            export_folder = os.path.join(root_dir + '_export/img-cg/' + getExportName() + '.fbx');
            return export_folder

        elif dir_list[4] in ["_SHOTS"]:
            dir_list = dir_list[:-3];
            root_dir = '/'.join(dir_list) + '/';
            scene_name = cmds.file(q=True, shortName=True, sceneName=True);
            file_name = scene_name.split(".")[-2]
            export_folder = os.path.join(root_dir + '_export/img-cg/' + getExportName() + '.fbx' );
            return export_folder

    return None

def getExportName():

    filepath = cmds.file(q=True, sn=True)
    if filepath:
        filename = os.path.basename(filepath)
        filename_without_ext = filename.split(".")[-2]
        filename_parts = filename_without_ext.split("_")

        import sgtk
        eng = sgtk.platform.current_engine()
        eng.context

        # for file Name
        # scene_name = cmds.file(q=True, shortName=True, sceneName=True);
        # file_name = scene_name.split(".")[-2]

        # for file Name with playplast
        if len(filename_parts) == 5:  # Example TST_0000_cmp_v002_dr069
            fbxExport_name = filename_parts[0] + "_" + filename_parts[1] + "_" + filename_parts[2] + "_fbx_" + \
                             filename_parts[3] + "_" + filename_parts[4]
        elif len(filename_parts) == 4:  # Example DRTPRP_mod_v001_dr069
            fbxExport_name = filename_parts[0] + "_" + filename_parts[1] + "_fbx_" + \
                             filename_parts[2] + "_" + filename_parts[3]
        else:
            fbxExport_name = " _fbx_ "

        return fbxExport_name  # file_name
    return None