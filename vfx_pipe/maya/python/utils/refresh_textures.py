import maya.cmds as cmds
import os
import fnmatch


def refresh_textures():
    filenodes = cmds.ls(et="file")

    for f in filenodes:
        currentFile = cmds.getAttr("%s.fileTextureName" % f)
        currentFileName = os.path.basename(currentFile)
        dir = os.path.dirname(currentFile)
        split = currentFileName.rsplit('_')
        version = split[-2][1:]
        max_version = 0

        file_list = os.listdir(dir)
        for fileItem in file_list:
            r = fileItem.rsplit('_')
            if len(r) < 3:
                pass
            elif max_version < r[-2][1:]:
                print(r)
                max_version = r[-2][1:]
        filename = ""
        for i in split[:-2]:
            filename = filename + i + "_"
        filename = filename + "v" + max_version + "*"

        for fileItem in file_list:
            if fnmatch.fnmatch(fileItem, filename):
                newFile = os.path.join(dir, fileItem)
                print(newFile)
                cmds.setAttr("%s.fileTextureName" % f, newFile, type="string")
