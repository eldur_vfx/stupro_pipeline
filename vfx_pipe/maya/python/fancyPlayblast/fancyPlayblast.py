import maya.cmds as cmds
import maya.mel as mel
import os
import re


def showFancyPlayblast():
    win_name = 'fancyPlayblast'
    if cmds.windowPref(win_name, exists=True):
        cmds.windowPref(win_name, remove=True)
    if cmds.window(win_name, exists=True):
        cmds.deleteUI(win_name)

    # Get dir and filename for Playblast
    playblast_name = getPlayblastName()
    playblast_dir = getPlayblastDir()
    start = cmds.playbackOptions(q=True, min=True)
    end = cmds.playbackOptions(q=True, max=True)

    # Set UI for Playblast
    cmds.window(win_name, t="Fancy Playblast Tool", wh=(150, 100), rtf=1)
    cmds.columnLayout(adjustableColumn=True, columnAttach=["both", 5], rowSpacing=8, columnWidth=100)
    cmds.textFieldGrp("fileName", label='File', text=playblast_name)
    cmds.textFieldGrp("dirName", label='Directory', text=playblast_dir)
    cmds.intFieldGrp("from_frame", numberOfFields=1, label='From Frame', value1=start)
    cmds.intFieldGrp("to_frame", numberOfFields=1, label='To Frame', value1=end)
    cmds.button("playblastButton", label='Playblast',
                command="import fancyPlayblast.fancyPlayblast; fancyPlayblast.fancyPlayblast.renderFancyPlayblast()")
    # cmds.button("openFolder", label='Open Playblast Folder',
    #             command="import fancyPlayblast.fancyPlayblast; fancyPlayblast.fancyPlayblast.open_dir()")

    cmds.showWindow(win_name)


def renderFancyPlayblast():
    f = cmds.textFieldGrp("fileName", text=True, query=True)
    dir = cmds.textFieldGrp("dirName", text=True, query=True)
    from_frame = cmds.intFieldGrp("from_frame", value1=True, query=True)
    to_frame = cmds.intFieldGrp("to_frame", value1=True, query=True)
    file_path = os.path.join(dir, f)

    sound_node = mel.eval("timeControl -q -s $gPlayBackSlider")

    if not os.path.isdir(dir):
        os.makedirs(dir)

    # will overwrite file if one is already there
    # CP3
    #cmds.playblast(format='avi', filename=file_path, widthHeight=(2048, 1152), st=from_frame, et=to_frame, clearCache=1, viewer=0,
    #               showOrnaments=0, fp=4, percent=100, sound=sound_node, fo=1, qlt=20, compression='IYUV Codec')
    # Sigma 2083, 1171 = 2048, 1151
    cmds.playblast(format='avi', filename=file_path, widthHeight=(2048, 1151), st=from_frame, et=to_frame, clearCache=1, viewer=0,
                  showOrnaments=0, fp=4, percent=80, sound=sound_node, fo=1, qlt=20, compression='IYUV Codec')

def getPlayblastDir():
    workfolder = mel.eval("workspace -q -rd;");
    if workfolder:
        dir_list = workfolder.split('/');

        if dir_list[4] in ["_ASSETS"]:
            dir_list = dir_list[:-4];
            root_dir = '/'.join(dir_list) + '/';
            scene_name = cmds.file(q=True, shortName=True, sceneName=True);
            file_name = scene_name.split(".")[-2]
            export_folder = os.path.join(root_dir + '_export/img-prv/' + file_name + '/');
            return export_folder

        elif dir_list[4] in ["_SHOTS"]:
            dir_list = dir_list[:-3];
            root_dir = '/'.join(dir_list) + '/';
            scene_name = cmds.file(q=True, shortName=True, sceneName=True);
            file_name = scene_name.split(".")[-2]
            export_folder = os.path.join(root_dir + '_export/img-prv/' + file_name + '/');
            return export_folder

    return None



def getPlayblastName():



    filepath = cmds.file(q=True, sn=True)
    if filepath:
        filename = os.path.basename(filepath)
        filename_without_ext = filename.split(".")[-2]
        filename_parts = filename_without_ext.split("_")

        import sgtk
        eng = sgtk.platform.current_engine()
        eng.context

        #for file Name
        #scene_name = cmds.file(q=True, shortName=True, sceneName=True);
        #file_name = scene_name.split(".")[-2]

        #for file Name with playplast
        if len(filename_parts) == 5 : # Example TST_0000_cmp_v002_dr069
            playblast_name = filename_parts[0] + "_" + filename_parts[1] + "_" + filename_parts[2] + "_playblast_" + \
                             filename_parts[3] + "_" + filename_parts[4]
        elif len(filename_parts) == 4 : # Example DRTPRP_mod_v001_dr069
            playblast_name = filename_parts[0] + "_" + filename_parts[1] + "_playblast_" + \
                             filename_parts[2] + "_" + filename_parts[3]
        else:
            playblast_name =" _playblast_ "

        return playblast_name #file_name
    return None


def openDir():
    direct = get_dir()
    os.startfile(direct)
    cmds.deleteUI("fancyPlayblast")
