# import pyblish.api
# import pymel.core as pm
# import re
#
#
# try:
#   import sgtk
#   eng = sgtk.platform.current_engine()
# except Exception as e:
#   eng = None
#
# if eng is not None:
#   entity = str(eng.context).split(" ")[-1]
#   entitiy_step = eng.context.step["name"]
#   entitiy_type = eng.context.entity["type"]
# else:
#   entitiy = ""
#
# pattern = entity + '_RIG_GRP'
# pattern_main = entity + "_MAIN_GRP"
#
# scene_transforms = pm.ls(type="transform")
#
# class CollectSceneRiggs(pyblish.api.ContextPlugin):
#
#   order = pyblish.api.CollectorOrder  # <-- This is new
#   name = "Scene Rigs"
#
#   def process(self, context):
#     if entitiy_type == "Asset" and entitiy_step == "rigging":
#       found_parent = False
#       for transform in scene_transforms:
#         if pattern == transform.stripNamespace():
#           entitiy_children = pm.listRelatives(transform, allDescendents =True)
#           context.create_instance(name=transform.name(), family="rig", icon="steam", valid=True)
#           found_parent = True
#
#       if not found_parent:
#         if len(pm.ls(type="nurbsCurve")) != 0:
#           context.create_instance(name="Unassigned Controls", family="rig", icon="steam", valid=False)
#
#
# class ValidateSceneRigs(pyblish.api.InstancePlugin):
#
#   order = pyblish.api.ValidatorOrder
#   families = ["rig"]
#   optional = True
#
#   def process(self, instance):
#     assert instance.data["valid"], "Couldn't find rig parent group. Should be called: {}".format(pattern)
#
#     if entitiy_type == "Asset" and entitiy_step == "rigging":
#         main_grp = False
#         for parent in pm.listRelatives(instance.data["name"], parent=True):
#           if pattern_main in parent.name():
#             main_grp = True
#
#         assert main_grp, "{} should be in {}.".format(pattern, pattern_main)
#
#