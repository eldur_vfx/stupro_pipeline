import pyblish.api
import pymel.core as pm

try:
    import sgtk

    eng = sgtk.platform.current_engine()
except Exception as e:
    sgeng = None

renderSettings_dict = {
    "getAttr": {

        # "defaultResolution.width" :  2880,
        # "defaultResolution.height" :  1620,
        "defaultRenderGlobals.animation": 1,
        "defaultRenderGlobals.putFrameBeforeExt": 1,
        "defaultRenderGlobals.extensionPadding": 4,

        ##rs107: errors for some reason?
        # "defaultRenderGlobals.enableDefaultLight", 0,
        "defaultResolution.pixelAspect": 1,
        "defaultResolution.aspectLock": 1,
        ##rs107: errors for some reason?
        # "redshiftOptions.exrBits": 32,
        # "redshiftOptions.imageFormat", 1,
        # "redshiftOptions.exrForceMultilayer", 1,
        # "redshiftOptions.primaryGIEngine", 4,
        # "redshiftOptions.secondaryGIEngine", 2,
        # "rsAov_Cryptomatte.enabled", 1,
        # "rsAov_DiffuseFilter.enabled", 1,
        # "rsAov_Depth.enabled", 1,
        # "rsAov_DiffuseLighting.enabled", 1,
        # "rsAov_DiffuseLighting.globalAov", 2,
        # "rsAov_DiffuseLighting.allLightGroups", 1,
        # "rsAov_Emission.enabled", 1,
        # "rsAov_GlobalIllumination.enabled", 1,
        # "rsAov_MotionVectors.enabled", 1,
        # "rsAov_Normals.enabled", 1,
        # "rsAov_Reflections.enabled", 1,
        # "rsAov_ReflectionsFilter.enabled", 1,
        # "rsAov_Refractions.enabled", 1,
        # "rsAov_RefractionsFilter.enabled", 1,
        # "rsAov_WorldPosition.enabled", 1
    },
    "colorManagementPrefs": {
        "cmEnabled": 1,
        "renderingSpaceName": "ACES - ACEScg"
    },
    "currentUnit": {
        "time": "film"
    }
}


class FixAffectedSetting(pyblish.api.Action):
    label = "Fix Render Settings"
    on = "failed"

    def process(self, context, plugin):
        for result in context.data["results"]:
            if result["error"] is None:
                continue
            else:
                self.fixRenderSettings()

            self.log.info("Fixing all render settings")

    def fixRenderSettings(self):
        # pm.setAttr("defaultResolution.width", 2880)
        # pm.setAttr("defaultResolution.height", 1620)
        # pm.setAttr("defaultRenderGlobals.animation", 1)
        pm.setAttr("defaultRenderGlobals.putFrameBeforeExt", 1)
        pm.setAttr("defaultResolution.pixelAspect", 1)
        # pm.setAttr("defaultRenderGlobals.extensionPadding", 4)

        # pm.setAttr("redshiftOptions.imageFormat", 1)
        # pm.setAttr("redshiftOptions.exrBits", 32)
        # pm.setAttr("redshiftOptions.exrForceMultilayer", 1)
        # pm.setAttr("defaultRenderGlobals.enableDefaultLight", 0)
        # pm.setAttr("redshiftOptions.primaryGIEngine", 4)
        # pm.setAttr ("rsAov_Cryptomatte.enabled", 1)
        # pm.setAttr ("rsAov_DiffuseFilter.enabled", 1)
        # pm.setAttr ("rsAov_Depth.enabled", 1)
        # pm.setAttr ("rsAov_DiffuseLighting.enabled", 1)
        # pm.setAttr ("rsAov_DiffuseLighting.globalAov", 2)
        # pm.setAttr ("rsAov_DiffuseLighting.allLightGroups", 1)
        # pm.setAttr ("rsAov_Emission.enabled", 1)
        # pm.setAttr ("rsAov_GlobalIllumination.enabled", 1)
        # pm.setAttr ("rsAov_MotionVectors.enabled", 1)
        # pm.setAttr ("rsAov_Normals.enabled", 1)
        # pm.setAttr ("rsAov_Reflections.enabled", 1)
        # pm.setAttr ("rsAov_ReflectionsFilter.enabled", 1)
        # pm.setAttr ("rsAov_Refractions.enabled", 1)
        # pm.setAttr ("rsAov_RefractionsFilter.enabled", 1)
        # pm.setAttr ("rsAov_WorldPosition.enabled", 1)

        pm.currentUnit(time="film")

        pm.colorManagementPrefs(e=1, cmEnabled=1)
        pm.colorManagementPrefs(e=1, renderingSpaceName="ACES - ACEScg")


class CollectRenderSettings(pyblish.api.ContextPlugin):
    order = pyblish.api.CollectorOrder  # <-- This is new

    def process(self, context):
        name = "Render Settings"
        context.create_instance(name=name, family="settings", icon="cogs")


class ValidateRenderSettings(pyblish.api.InstancePlugin):
    order = pyblish.api.ValidatorOrder
    optional = True
    families = ["settings"]
    actions = [FixAffectedSetting]

    def process(self, instance):
        pm.mel.python("import pymel.core as pm")
        for function, settings in renderSettings_dict.items():
            for setting, value in settings.items():
                if function == "getAttr":
                    current_value = pm.getAttr(setting)
                    assert value == current_value, "Wrong setting for {}. Setting is {}, should be {}".format(
                        setting,
                        current_value,
                        value
                    )
                elif function == "colorManagementPrefs":
                    current_value = pm.mel.python("pm.colorManagementPrefs(q=1, {}=True)".format(setting))
                    assert value == current_value, "Wrong setting for {}. Setting is {}, should be {}".format(
                        str(setting + "." + function),
                        current_value,
                        value
                    )
                elif function == "currentUnit":
                    current_value = pm.mel.python("pm.currentUnit(q=1, {}=1)".format(setting))
                    assert value == current_value, "Wrong setting for {}. Setting is {}, should be {}".format(
                        str(setting + "." + function),
                        current_value,
                        value
                    )
