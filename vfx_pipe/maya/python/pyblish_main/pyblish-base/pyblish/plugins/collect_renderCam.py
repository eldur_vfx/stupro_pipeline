import pyblish.api
import pymel.core as pm

try:
    import sgtk

    eng = sgtk.platform.current_engine()
except Exception as e:
    eng = None

if eng is not None:
    entity = str(eng.context).split(" ")[-1]
    entitiy_step = eng.context.step["name"]
    entitiy_type = eng.context.entity["type"]
else:
    entitiy = ""


class FixRenderCam(pyblish.api.Action):
    label = "Fix Render Cam"
    on = "failed"
    families = ["camera"]

    def process(self, context, plugin):
        for result in context.data["results"]:
            if result["error"] is None:
                continue
            else:
                for cam in pm.ls(type="camera"):
                    cam.renderable.set(0)
                if "camera" in result["instance"].data["family"]:
                    pm.PyNode(result["instance"].data["name"]).getShape().renderable.set(1)

            self.log.info("Fixing render cam")


# class CollectCameraInstances(pyblish.api.ContextPlugin):
#     order = pyblish.api.CollectorOrder  # <-- This is new
#     name = "Camera Instances"
#
#     def process(self, context):
#
#         if eng is not None:
#
#             entity = str(eng.context).split(" ")[-1]
#
#             mm_cam_name_pattern = "_".join([entity, "mm", "renderCam"])
#             an_cam_name_pattern = "_".join([entity, "an", "renderCam"])
#
#             tt_cam_name_pattern = "_".join([entity, "tt", "renderCam"])
#
#             if entitiy_step == "texturing":
#                 cam_names = [tt_cam_name_pattern]
#             elif entitiy_step == "lighting shading rendering":
#                 cam_names = [mm_cam_name_pattern]
#             elif entitiy_step == "animation":
#                 cam_names = [an_cam_name_pattern]
#             elif entitiy_step == "matchmove":
#                 cam_names = [mm_cam_name_pattern]
#             else:
#                 cam_names = []
#
#             for cam_name in cam_names:
#                 matching = [str(pm.PyNode(s.name()).getParent()) for s in pm.ls(type="camera") if
#                             cam_name == s.stripNamespace()]
#                 if len(matching) == 0:
#                     matching = [str(pm.PyNode(s.name())) for s in pm.ls(type="transform") if
#                                 cam_name == s.stripNamespace()]
#                     for name in matching:
#                         context.create_instance(name=name, family="camera", icon="video-camera")
#                 else:
#                     assert len(matching) == 0, "Szene is missing: {}".format(cam_name)


class ValidateCameraSettings(pyblish.api.InstancePlugin):
    order = pyblish.api.ValidatorOrder
    families = ["camera"]
    optional = True
    actions = [FixRenderCam]

    def process(self, instance):
        name = instance.data["name"]

        renderable = pm.PyNode(name).renderable.get()
        assert renderable, "{} is not set as renderable camera".format(name)

        for camera in pm.ls(type="camera"):
            if str(camera.getParent().name()) != name:
                assert not pm.PyNode(camera).renderable.get(), "{} is not set as only renderable camera".format(name)
                c_scale = camera.getParent().scale.get()
                x, y, z = c_scale[0], c_scale[1], c_scale[2]
                assert .9 < x < 1.1 and .9 < y < 1.1 and .9 < z < 1.1, "{} scale is not normalized: {}".format(name,
                                                                                                               [x, y,
                                                                                                                z])


class BakeRenderCam(pyblish.api.InstancePlugin):
    order = pyblish.api.ExtractorOrder
    families = ["camera"]
    optional = True

    def process(self, instance):
        if entitiy_type == "Shot":
            name = instance.data["name"]

            node = pm.PyNode(name)

            new_cam = pm.duplicate(node, name=node.stripNamespace() + "_baked")[0]
            pm.parent(new_cam, w=1)

            constraint = pm.parentConstraint(node, new_cam)
            start = pm.playbackOptions(q=1, ast=1)
            end = pm.playbackOptions(q=1, aet=1)

            pm.bakeResults(new_cam, t=(start, end))

            pm.delete(constraint)

            node.renderable.set(0)
            new_cam.renderable.set(1)

            self.log.info("Baked {} to world.".format(name))
