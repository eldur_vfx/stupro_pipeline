import os
import sys


def load_pyblish():
    pipeline_path = os.getenv("PIPE_PIPELINEPATH")
    pyblish_path = os.path.join(pipeline_path, "maya", "python", "pyblish_main")
    pyblish_base_path = os.path.join(pyblish_path, "pyblish-base")
    pyblish_maya_path = os.path.join(pyblish_path, "pyblish-maya")
    pyblish_lite_path = os.path.join(pyblish_path, "pyblish-lite")
    pyblish_qml_path = os.path.join(pyblish_path, "pyblish-qml")
    python_path = os.path.join(pipeline_path, "Python39", "python.exe")

    sys.path.append(pyblish_base_path)
    sys.path.append(pyblish_maya_path)
    sys.path.append(pyblish_lite_path)
    sys.path.append(pyblish_qml_path)

    import pyblish.api
    import pyblish_maya
    import pyblish_qml.api

    os.environ["PYTHONPATH"] += os.pathsep + os.sep.join(pyblish_qml.__file__.rsplit(os.sep)[:-2])
    os.environ["PYTHONPATH"] += os.pathsep + os.sep.join(pyblish.__file__.rsplit(os.sep)[:-2])

    pyblish_maya.teardown()

    # 0. Tell pyblish-qml where to find python
    pyblish_qml.api.register_python_executable(python_path)

    # 1. Register your favourite GUI
    try:
        pyblish.api.register_gui("pyblish_lite")
        # pyblish.api.register_gui("pyblish_qml")
    except Exception as e:
        print(e)
        pyblish.api.register_gui("pyblish_lite")
        # pyblish.api.register_gui("pyblish_qml")

    # 2. Set-up Pyblish for Maya
    pyblish_maya.setup(menu=False)
    pyblish_maya.show()
