
from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui
from shiboken2 import wrapInstance
import maya.api.OpenMaya as om

import maya.OpenMayaUI as omui
import pymel.core as pm
import maya.app.renderSetup.model.renderSetup as renderSetup
import os
import subprocess
from subprocess import call
import random
import gc
import threading 
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
import sys
import maya.cmds as cmds
import gc
import render_checker.skRenderChecker
import json


def maya_main_window():
    """
    Return the Maya main window widget as a Python object
    """
    main_window_ptr = omui.MQtUtil.mainWindow()
    try:
        return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)
    except:
        return None


class Layer_Exporter(MayaQWidgetDockableMixin, QtWidgets.QMainWindow):

    JSON_PATH = os.path.join(os.getenv("PIPE_PIPELINEPATH"), "PIPELINE_CONSTANTS.json")

    def __init__(self, parent=maya_main_window()):
        super(Layer_Exporter, self).__init__(parent)

        self.MAYA_VERSION = pm.versions.shortName()

        self.setWindowTitle("skRenderSubmitter")
        self.setFixedWidth(300)
        self.setMinimumHeight(580)
        self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowContextHelpButtonHint)
        
        self.CHECK_STATUS = False

        self.central_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.central_widget)

        self.create_widgets()
        self.create_layouts()
        self.create_connections()
        self.get_layers()
        self.fill_layer_layout()
        self.get_platform()
        self.set_initial_state()

    def get_platform(self):

        platform = sys.platform
        self.mayapy_path = sys.executable

        if platform == "darwin":
            self.mayapy_path = '"/Applications/Autodesk/maya{}/Maya.app/Contents/bin/mayapy"'.format(self.MAYA_VERSION)
            self.plugin_extension = ".bundle"
        elif platform == "win32":
            self.mayapy_path = '"C:/Program Files/Autodesk/Maya{}/bin/mayapy.exe"'.format(self.MAYA_VERSION)
            self.plugin_extension = ".mll"
        elif platform == "linux":
            pass

    def create_widgets(self):
        self.description_label = QtWidgets.QLabel("To Use:\nSelect Render-Layers and output directory\nSelect Sequence and input frame range and step\nSelect render job, set name, output directory and prio\n\nAutomatically exports proxy scenes and\ncreates RenderPal NetJob")
        
        folder_icon = QtGui.QIcon(":/openLoadGeneric.png")
        arrow_icon = QtGui.QIcon(":/list.svg")

        self.layer_title_label = QtWidgets.QLabel("Layers:\n")
        self.output_directory_dialog = QtWidgets.QFileDialog()
        
        self.sequence_label = QtWidgets.QLabel("Sequence: ")
        self.sequence_checkbox = QtWidgets.QCheckBox()
        self.sequence_checkbox.setChecked(True)
        self.start_frame_label = QtWidgets.QLabel("Start: ")
        self.start_frame_label.setMaximumWidth(50)
        self.end_frame_label = QtWidgets.QLabel("End: ")
        self.end_frame_label.setMaximumWidth(50)
        self.step_label = QtWidgets.QLabel("Step: ")
        self.step_label.setMaximumWidth(50)
        self.start_frame_lineedit = QtWidgets.QLineEdit(str(pm.playbackOptions(q=1, min=1)))
        self.start_frame_lineedit.setValidator(QtGui.QDoubleValidator())
        self.start_frame_lineedit.setMaximumWidth(50)
        self.end_frame_lineedit = QtWidgets.QLineEdit(str(pm.playbackOptions(q=1, max=1)))
        self.end_frame_lineedit.setValidator(QtGui.QDoubleValidator())
        self.end_frame_lineedit.setMaximumWidth(50)
        self.step_lineedit = QtWidgets.QLineEdit("1")
        self.step_lineedit.setMaximumWidth(50)
        self.step_lineedit.setValidator(QtGui.QIntValidator())
        
        self.submitter_label = QtWidgets.QLabel("Submit To Render:")
        self.submitter_checkbox = QtWidgets.QCheckBox()
        self.submitter_checkbox.setChecked(True)
        self.job_name_label = QtWidgets.QLabel("Job Name:")
        self.job_name_lineedit = QtWidgets.QLineEdit()
        self.prio_label = QtWidgets.QLabel("Prio:")
        self.prio_lineedit = QtWidgets.QLineEdit("5")
        self.prio_lineedit.setMaximumWidth(50)
        self.render_output_dir_lineedit = QtWidgets.QLineEdit()
        self.render_output_dir_lineedit.setPlaceholderText("Render output directory.")
        self.render_output_dir_button = QtWidgets.QPushButton()
        self.render_output_dir_button.setIcon(folder_icon)
        self.render_output_dir_button.setMinimumWidth(40)
        self.send_dir_to_checker_button = QtWidgets.QPushButton()
        self.send_dir_to_checker_button.setIcon(arrow_icon)
        self.send_dir_to_checker_button.setMinimumWidth(20)
        self.send_dir_to_checker_button.setToolTip("Send render directory to skRenderChecker")
        self.render_directory_dialog = QtWidgets.QFileDialog()
        
        self.render_check_label = QtWidgets.QLabel("Check Frames:")
        self.render_check_checkbox = QtWidgets.QCheckBox()
        
        self.submit_button = QtWidgets.QPushButton("Submit")
        self.cancel_button = QtWidgets.QPushButton("Cancel")

        self.setStyleSheet(r"QPushButton:hover{background: #278cbc; color: black}")


    def create_layouts(self):

        layer_box = QtWidgets.QGroupBox("Layers:")
        layer_box_layout = QtWidgets.QVBoxLayout()
        layer_box.setStyleSheet(r"QGroupBox {font: bold 14px}")
        scroll_area = QtWidgets.QScrollArea()        
        central_widget = QtWidgets.QWidget()   
        self.layer_layout = QtWidgets.QVBoxLayout(central_widget)
        self.layer_layout.addStretch()   
        scroll_area.setWidget(central_widget)  
        scroll_area.setWidgetResizable(True)  
        layer_box_layout.addWidget(scroll_area)       
        layer_box.setLayout(layer_box_layout)
                      
        render_box = QtWidgets.QGroupBox("Render:")
        render_box.setStyleSheet(r"QGroupBox {font: bold 14px}")  
        render_layout = QtWidgets.QVBoxLayout()
        render_layout.setSpacing(10)
        render_layout_01 = QtWidgets.QHBoxLayout()
        render_layout_01.addWidget(self.submitter_label)
        render_layout_01.addWidget(self.submitter_checkbox) 
        render_layout_01.addWidget(self.render_check_label)
        render_layout_01.addWidget(self.render_check_checkbox)
        render_layout_01.addStretch()    
        render_layout_05 = QtWidgets.QVBoxLayout()
        render_layout_05.setSpacing(10)
        render_layout_05 = QtWidgets.QHBoxLayout()
        render_layout_05.addWidget(self.sequence_label)
        render_layout_05.addWidget(self.sequence_checkbox)
        render_layout_05.addStretch()
        render_layout.addLayout(render_layout_05)
        render_layout_06 = QtWidgets.QHBoxLayout()
        render_layout_06.addWidget(self.start_frame_label)
        render_layout_06.addWidget(self.start_frame_lineedit)
        render_layout_06.addWidget(self.end_frame_label)
        render_layout_06.addWidget(self.end_frame_lineedit)
        render_layout_06.addWidget(self.step_label)
        render_layout_06.addWidget(self.step_lineedit)
        render_layout.addLayout(render_layout_06)   
        render_layout_02 = QtWidgets.QHBoxLayout()
        render_layout_02.addWidget(self.job_name_label)
        render_layout_02.addWidget(self.job_name_lineedit)
        render_layout_02.addWidget(self.prio_label)
        render_layout_02.addWidget(self.prio_lineedit) 
        render_layout_02.addStretch() 
        render_layout_03 = QtWidgets.QHBoxLayout()
        render_layout_03.addWidget(self.render_output_dir_lineedit)
        render_layout_03.addWidget(self.render_output_dir_button)  
        render_layout_03.addWidget(self.send_dir_to_checker_button)  
        render_layout_04 = QtWidgets.QHBoxLayout()
        render_layout_04.addStretch()
        render_layout.addLayout(render_layout_01)
        render_layout.addLayout(render_layout_02)
        render_layout.addLayout(render_layout_03)
        render_layout.addLayout(render_layout_04)
        render_box.setLayout(render_layout)
                
        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addWidget(self.submit_button)
        button_layout.addWidget(self.cancel_button)
        
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addWidget(self.description_label)
        main_layout.addSpacing(10)
        main_layout.addWidget(layer_box)
        main_layout.addSpacing(10)
        main_layout.addSpacing(10)
        main_layout.addWidget(render_box)
        main_layout.addSpacing(10)
        main_layout.addLayout(button_layout)
        main_layout.addStretch()

        self.central_widget.setLayout(main_layout)
        
        
    def create_connections(self):
        self.cancel_button.clicked.connect(self.close)
        self.submit_button.clicked.connect(self.set_initial_check_status)
        self.submit_button.clicked.connect(self.submit)
        self.render_output_dir_button.clicked.connect(self.get_render_directory)
        self.send_dir_to_checker_button.clicked.connect(self.send_path_to_checker)

    def submit(self):
        active_layers = self.get_active_layer_names()
        command = self.create_net_job(pm.sceneName(), active_layers)
        call(command)


    def get_active_layer_names(self):
        active_layers = []
        
        for render_layer, items in self.render_layer_dict.items():
            if items[1].isChecked():
                active_layers.append(render_layer)
        return active_layers

              
    def get_layers(self):
        render_setup = renderSetup.instance()
        render_layers = render_setup.getRenderLayers()
        default_layer = render_setup.getDefaultRenderLayer()
        self.active_render_layer = render_setup.getVisibleRenderLayer()
        self.render_layer_dict = {}
        
        self.render_layer_dict[default_layer.name()] = [default_layer]
        
        for render_layer in render_layers:
            render_layer_name = render_layer.name()
            
            self.render_layer_dict[render_layer_name] = [render_layer]
            
    def fill_layer_layout(self):
        for layer_name in self.render_layer_dict.keys():
            layer_selector = self.make_layer_selector(layer_name)
            
            self.layer_layout.addLayout(layer_selector)
        
    def make_layer_selector(self, layer_name):
        selector_layout = QtWidgets.QHBoxLayout()
        selector_label = QtWidgets.QLabel(layer_name)
        selector_checkbox = QtWidgets.QCheckBox()
        
        selector_layout.addWidget(selector_label)
        selector_layout.addWidget(selector_checkbox)
        
        self.render_layer_dict[layer_name].append(selector_checkbox)
        selector_label.setStyleSheet("QLabel { background: #278cbc; color : black; border: 1px solid #278cbc; border-radius: 2px; padding: 2px 2px 2px 2px; border }")
        
        return selector_layout
        
    def get_output_directory(self):
        self.output_directory = self.output_directory_dialog.getExistingDirectory()
        
    def get_render_directory(self):
        self.render_directory = self.render_directory_dialog.getExistingDirectory()
        self.render_output_dir_lineedit.setText(self.render_directory)
                
    def create_check_dialog(self):
        check_dialog = QtWidgets.QDialog(self)
        check_dialog.setWindowFlags(check_dialog.windowFlags() ^ QtCore.Qt.WindowContextHelpButtonHint)
        check_dialog.setWindowTitle("Check Frames")
        check_dialog.setMinimumWidth(200)
        
        ok_btn = QtWidgets.QPushButton("OK")
        check_btn = QtWidgets.QPushButton("Check")
        cancel_btn = QtWidgets.QPushButton("Cancel")
        
        info_label = QtWidgets.QLabel("Please check test-frames and confirm.")
        
        main_layout = QtWidgets.QVBoxLayout(check_dialog)
        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addWidget(check_btn)
        button_layout.addWidget(ok_btn)
        button_layout.addWidget(cancel_btn)
        main_layout.addWidget(info_label)
        main_layout.addLayout(button_layout)
        check_dialog.show()
        
        self.CHECK_STATUS = False
        ok_btn.clicked.connect()
        ok_btn.clicked.connect(check_dialog.close) 
        check_btn.clicked.connect(self.open_check_dirs)    
        cancel_btn.clicked.connect(check_dialog.close)
        
    def open_check_dirs(self):
        for dir in self.layer_export_list:
            for item in os.listdir(dir):
                command = '"C:\\Program Files\\djv-1.1.0-Windows-64\\bin\\djv_view.exe" "{}"'.format(dir+"\\"+item)
                call(command, shell=True)
            
    def set_initial_check_status(self):
        self.CHECK_STATUS = self.render_check_checkbox.isChecked()

    def set_initial_state(self):

        render_dir, ass_dir = self.get_info_from_path()

        self.render_output_dir_lineedit.setText(render_dir)
        #self.render_output_dir_lineedit.setDisabled(True)

    def get_info_from_path(self):
        scene_path = pm.sceneName()
        if self.get_project() in scene_path:
            scene_name = os.path.split(scene_path)[-1].split(".")[0]
            dir_path = os.path.dirname(scene_path)

            user = scene_name.split("_")[-1]
            version = scene_name.split("_")[-2]
            step = scene_name.split("_")[-3]
            shot = "_".join(scene_name.split("_")[0:2])

            cg_dir = dir_path.split("/work")[0]

            root_dir = "/".join([cg_dir, "_export", "img-cg", "work", scene_name])

            render_dir =  "/".join([root_dir, "render"])
            ass_dir =  "/".join([root_dir, "ass"])

            return render_dir, ass_dir
        else:
            return "", ""

    def get_project(self):
        with open(self.JSON_PATH, "r") as file:
            data = json.load(file)

        project_name = data["SG_PROJECTNAME"]

        return project_name           
   
    def check_sequence(self):
        if self.CHECK_STATUS == False:
            if self.sequence_checkbox.isChecked():
                return float(self.start_frame_lineedit.text()), float(self.end_frame_lineedit.text()), float(self.step_lineedit.text())
            else:
                return None, None, None
        if self.CHECK_STATUS == True:
            middle_frame = int((float(self.start_frame_lineedit.text())+ float(self.end_frame_lineedit.text()))/2)
            return middle_frame, middle_frame, 1
            
    def create_directory(self, path):
        if not os.path.isdir(path):
            os.makedirs(path)
            
    def get_file_list(self, path):
        files = os.listdir(path)
        file_path_list = []
        for item in files:
            file_path_list.append('"{}"'.format(path + "/" + item))
        return file_path_list
           
    def create_net_job(self, scene_path, layers):
                
        job_name = self.job_name_lineedit.text()
        job_prio = int(self.prio_lineedit.text())
        job_out_dir = self.render_output_dir_lineedit.text()

        self.create_directory(job_out_dir)

        dummy_scene_path = "Z:/_pipeline/master/hdmvfx-pipeline/pipeline/maya/dummy.ma"
                               
        command = (" ").join([
        '"C:/Program Files (x86)/RenderPal V2/CmdRC/rprccmd"',
        '-nj_name "{name}"',
        '-nj_priority {prio}',
        '-login "render:render"',
        '-server "ca-lic"',
        '-nj_renderer "Redshift_vfx/redshift_3"',
        '-nj_pools "vfx_redshift"',
        '-nj_tags "Getaway"',
        '-nj_splitmode "2, 3"',
        '-outdir "{job_out_dir}"',
        '-frames "{start}-{end}"',
        '-fstep "{step}"',
        '-rl "{layers}"',
        #'-preRender "python(\\\"{preRender_command}\\\");setMayaSoftwareLayers \\\"{layers}\\\" \\\"\\\""',
        '{scene_path}'])
        
        command = command.format(
        name = job_name,
        prio = job_prio,
        job_out_dir = job_out_dir,
        start = float(self.start_frame_lineedit.text()),
        end = float(self.end_frame_lineedit.text()),
        step = float(self.step_lineedit.text()),
        preRender_command = self.build_preRender_command(scene_path),
        layers = ",".join(layers),
        #scene_path = dummy_scene_path.replace("Z:/", "//vfx-nas01/vfx2/")),
        scene_path=scene_path
        )

        print command
        
        return command

    def send_path_to_checker(self):
        out_dir = self.render_output_dir_lineedit.text()

        for obj in gc.get_objects():
            if isinstance(obj, render_checker.skRenderChecker.skRenderChecker):
                obj.set_search_dir(out_dir)

    def build_preRender_command(self, scene_path):
        command = "print('#### Loading Scene File ####');import pymel.core as pm;pm.loadPlugin('loadUNCReferenceCommand.py');pm.repathUNC();pm.openFile('{}', force=True)".format(scene_path)
        return command
           
def main():

    try:
        layer_exporter.close() # pylint: disable=E0601
        layer_exporter.deleteLater()
    except:
        pass

    layer_exporter = Layer_Exporter()

    layer_exporter.show(dockable=True)
    