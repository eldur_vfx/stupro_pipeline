global proc VolumeTrailUpdateEnabled( string $node )
{
    int $UseComputeMode = `getAttr ($node+".ComputeMode")`;
    if ($UseComputeMode == 0) 
    {
        editorTemplate -dimControl $node "TrailLength" 0;
        editorTemplate -dimControl $node "Substeps" 0;
        editorTemplate -dimControl $node "Integration" 0;
        editorTemplate -dimControl $node "VdbAllGridNamesVel" 0;
        editorTemplate -dimControl $node "BoundingBox" 0;
        editorTemplate -dimControl $node "TrailLengthPoints" 1;
    }
    if ($UseComputeMode == 1)
    {
        editorTemplate -dimControl $node "TrailLength" 1;
        editorTemplate -dimControl $node "Substeps" 1;
        editorTemplate -dimControl $node "Integration" 1;
        editorTemplate -dimControl $node "VdbAllGridNamesVel" 1;
        editorTemplate -dimControl $node "BoundingBox" 1;
        editorTemplate -dimControl $node "TrailLengthPoints" 0;
    }
}

global proc AEBE_VDBVolumeTrailTemplate( string $nodeAttr )
{
    editorTemplate -beginScrollLayout;
    {
        editorTemplate -beginLayout "BE_VDB Volume Trail Settings" -collapse 0;
        editorTemplate -beginNoOptimize;
        {
        	editorTemplate -addControl "ComputeMode" "VolumeTrailUpdateEnabled"; 
            editorTemplate -callCustom "newBE_VDBVolumeTrailGridSelection" "replaceBE_VDBVolumeTrailGridSelection" "VdbAllGridNamesVel";

            editorTemplate -addSeparator;

            editorTemplate -addControl "BoundingBox" "VolumeTrailUpdateEnabled"; 
            editorTemplate -addControl "Curves";
            editorTemplate -addSeparator;
            editorTemplate -addControl "Integration" "VolumeTrailUpdateEnabled";
            editorTemplate -addControl "TrailLength" "VolumeTrailUpdateEnabled";
            editorTemplate -label "TrailLength" -addControl "TrailLengthPoints" "VolumeTrailUpdateEnabled";
            editorTemplate -addControl "Substeps" "VolumeTrailUpdateEnabled";
            editorTemplate -addSeparator;
            editorTemplate -addControl "UseRamp";
            editorTemplate -addControl "VAlueMode";
            AEaddRampControl( $nodeAttr + ".ColorRamp" );
            editorTemplate -addControl "Maximum";

        }
        editorTemplate -endNoOptimize;
        editorTemplate -endLayout;

        editorTemplate -beginLayout "Arnold Settings" -collapse 1;
        editorTemplate -beginNoOptimize;
        {
            editorTemplate -label "Mode" -addControl "aiMode"; 
            editorTemplate -label "Width" -addControl "aiCurveWidth";
            AEaddRampControl( $nodeAttr + ".aiWidthProfile" );
            editorTemplate -label "Sample Rate" -addControl "aiSampleRate"; 
            editorTemplate -label "Min Pixel Width" -addControl "aiMinPixelWidth";
            editorTemplate -label "Export Colors" -addControl "aiExportColors";  

        } 
        editorTemplate -endNoOptimize;
        editorTemplate -endLayout;

        editorTemplate -beginLayout "Render Settings" -collapse 1;
        editorTemplate -beginNoOptimize;
        {        
            editorTemplate -label "Casts Shadows" -addControl "castsShadows";
            editorTemplate -label "Receive Shadows" -addControl "receiveShadows";
            editorTemplate -label "Primary Visibility" -addControl "primaryVisibility";
            editorTemplate -label "Motion blur" -addControl "motionBlur";
            editorTemplate -label "Matte" -addControl "aiMatte";            
            editorTemplate -label "Opaque" -addControl "aiOpaque"; 

            editorTemplate -label "Visible In Diffuse Reflection" -addControl "aiVisibleInDiffuseReflection";
            editorTemplate -label "Visible In Specular Reflection" -addControl "aiVisibleInSpecularReflection";
            editorTemplate -label "Visible In Diffuse Transmission" -addControl "aiVisibleInDiffuseTransmission";
            editorTemplate -label "Visible In Specular Transmission" -addControl "aiVisibleInSpecularTransmission";
            editorTemplate -label "Visible In Volume" -addControl "aiVisibleInVolume";
            editorTemplate -label "Self Shadows" -addControl "aiSelfShadows";               

        } 
        editorTemplate -endNoOptimize;
        editorTemplate -endLayout;        

        AEdependNodeTemplate $nodeAttr;
	    editorTemplate -addExtraControls;
    }
    editorTemplate -endScrollLayout;
}

global proc newBE_VDBVolumeTrailGridSelection(string $attr)
{
    optionMenu
        -label "Velocity Grid Name:"
        -width 300
        -changeCommand ("updateBE_VDBVolumeTrailGridSelection( \""+$attr+"\" )")
        vdbGridNameMenuBE_VDBVolumeTrail;
        
    replaceBE_VDBVolumeTrailGridSelection($attr);
}

global proc replaceBE_VDBVolumeTrailGridSelection(string $attr)
{
    //connectControl vdbGridNameMenu $attr;
    
	$node = plugNode($attr);
    // fix changeCommand
    optionMenu -e -changeCommand ("updateBE_VDBVolumeTrailGridSelection( \""+$attr+"\" )") vdbGridNameMenuBE_VDBVolumeTrail;

    // save current item
    string $citem = getAttr ($node+".VdbSelectedGridNamesVel");
	
    // Clear old items
    {
        string $items[] = `optionMenu -q -ill vdbGridNameMenuBE_VDBVolumeTrail`;
        string $item;
        for ($item in $items) deleteUI $item;
    }

    // Add new items
    {
        string $currentGridNames = `getAttr $attr`;
        $currentGridNames = "* " + $currentGridNames;

        string $gridNames[];
        tokenize $currentGridNames " " $gridNames;

        string $name;
        for ($name in $gridNames) menuItem -l $name -parent vdbGridNameMenuBE_VDBVolumeTrail;
    }

    // restore current item
	if(`size($citem)` > 0)
    {
     optionMenu -e -value $citem vdbGridNameMenuBE_VDBVolumeTrail;
	}

   /// @todo re-select previous item if it exists, don't update VdbSelectedGridNames if the same item is selectd.
}

global proc updateBE_VDBVolumeTrailGridSelection(string $attr)
{
    string $selectedGrid = `optionMenu -q -value vdbGridNameMenuBE_VDBVolumeTrail`;
    string $selectionAttr = plugNode($attr) + ".VdbSelectedGridNamesVel"; 
    setAttr -type "string" $selectionAttr $selectedGrid;
}


