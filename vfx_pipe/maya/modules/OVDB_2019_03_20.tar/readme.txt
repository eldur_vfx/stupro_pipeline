OVDB (plug-in for Maya) © soup-dev LLC

installation
------------
1. Place the contents of OVDB.tar.gz in a desired directory.
2. For Windows run setup.exe, for Linux run setup (executable script).
3. Windows only! Install "Visual C++ Redistributable for Visual Studio 2015", or later version.

* Maya2017 specific - OVDB requires update 4, or later.

licensing
---------
1. Host ID is a specific piece of information which uniquely identifies a computer. It is required by the OVDB license generators to create valid licenses. There are 3 different ways to obtain the Host ID:
   3.1 Load OVDB in Maya, open Script Editor, look for line like this: "OVDB: Host ID (mac address): ############" <- this number is the Host ID of the computer.
   3.2 Load SOuP in Maya, click on SOuP/OVDB drop-down menu -> print host id.
   3.3 Use the standard OS commands:
       linux: ifconfig
       windows: ipconfig
2. To obtain a license:
   2.1 Purchase the type and amount of licenses from www.soup-dev.com/support.html
   2.2 Send email to sales@soup-dev.com that includes:
      - purchase order
      - for standalone licenses - host ids of the workstations that will run OVDB
      - for floating licenses - host id of the computer hosting the RLM license server
   2.3 Licenses are issued by email shortly after purchase.
3. Trial license:
   3.1 Trial license is valid for 15 days only. After that the OVDB plug-in will become inactive.
   3.2 Internet connection is required for OVDB to operate in this mode.
   3.3 License is obtained automatically without user intervention. Disregard #1 and #2.
4. Installing a standlone (node locked) license:
   4.1 Place the license file inside directory:
       linux:   /home/$USER/maya/
       windows: c:\users\$USER\documents\maya\
   4.2 If for some reason the license file has to be located inside another directory, use environment variable RLM_LICENSE to point to it (the directory).
5. Installing a floating license:
   5.1 Place the license file inside the RLM server directory.
   5.2 If for some reason the license file has to be located inside another directory, use environment variable RLM_LICENSE to point to it (the directory).
   5.3 Start the RLM server and make sure that no firewall is blocking it.
   5.4 License server administration:
      5.4.1 Open an internet browser on the computer where the RLM server is running and type "localhost:5054" in the URL field. This will show you the RLM administration options, where you can check the status of a license, see which computers are using licenses, or stop/start the server etc.
      5.4.2 To change the default RLM license port if other applications are already using it - edit the first line of your license file and change port 5053 to something else, then restart the RLM server.
      5.4.3 If there is an RLM server running, simply put the floating license file next to the other license files already in use and restart the server.
      5.4.4 For more detailed information visit http://www.reprisesoftware.com/RLM_Enduser.html
   5.5. If the RLM server and clients are on a different sub-networks, make the server visible by setting environment variable soupdev_LICENSE to its address and port, for exampe soupdev_LICENSE=192.168.5.5@1234.

getting started
---------------
1. Launch Maya.
2. Load the OVDB plug-in.

Node IDs
--------
0x00118A51 - 0x00118B23
