# Set Environments Variables on computer for a working Pipeline"

$script_path = split-path -parent $MyInvocation.MyCommand.Definition
Write-Host $script_path                                 # "D:/Nextcloud/pipeline/lovebirds_pipeline"
$pipeline_path = split-path -parent $script_path        # "D:/Nextcloud/pipeline"
$nextcloud_path = split-path -parent $pipeline_path     # "D:/Nextcloud"

# project relevant pathes
[Environment]::SetEnvironmentVariable("PIPE_PIPELINEPATH","$pipeline_path",'USER')
[Environment]::SetEnvironmentVariable("PIPE_PROJECTPATH","$nextcloud_path"+"\StuPro_vfx_WS20\_PROD",'USER')
[Environment]::SetEnvironmentVariable("PIPE_SHOTPATH","$nextcloud_path_path"+"\StuPro_vfx_WS20\_PROD\_SHOTS",'USER')
[Environment]::SetEnvironmentVariable("PIPE_ASSETPATH","$nextcloud_path"+"\StuPro_vfx_WS20\_PROD\_ASSETS",'USER')

# pyblish 
$PYBLISHPLUGINPATH=$script_path+"\maya\python\pyblish_main\plugins"
[Environment]::SetEnvironmentVariable("PYBLISHPLUGINPATH","$PYBLISHPLUGINPATH",'USER')
[Environment]::SetEnvironmentVariable("PYBLISH_GUI","$pyblish_qml",'USER')
[Environment]::SetEnvironmentVariable("PYBLISH_QML_PYTHON_EXECUTABLE","c:\python37\python.exe",'USER')

# maya
[Environment]::SetEnvironmentVariable("MAYA_LOCATION","C:\Program Files\Autodesk\Maya2020",'USER')
$MAYA_PLUG_IN_PATH=$script_path+"\maya"
[Environment]::SetEnvironmentVariable("MAYA_PLUG_IN_PATH","$MAYA_PLUG_IN_PATH",'USER')
# studiolibrary in maya
[Environment]::SetEnvironmentVariable("PATH", $env:Path + ";C:\Program Files\Autodesk\Maya2020\bin")
$STUDIO_LIBRARY_PATH=$script_path+"\maya\python\studiolibrary"
[Environment]::SetEnvironmentVariable("PATH", $env:Path + ";$STUDIO_LIBRARY_PATH", 'USER')

# nuke
$NUKE_PATH=$script_path+"\nuke"
[Environment]::SetEnvironmentVariable("NUKE_PATH","$NUKE_PATH",'USER')

# ocio
$OCIO=$script_path+"\ocio\configs\aces_1.0.3\config.ocio"
[Environment]::SetEnvironmentVariable("OCIO","$OCIO",'USER')

# shotgun
[Environment]::SetEnvironmentVariable("SHOTGUN_PATH_WINDOWS_StuPro_vfx_WS20","$stupro_path"+"\StuPro_vfx_WS20\_PROD",'USER')

[Environment]::SetEnvironmentVariable("VFX_NAS","$stupro_path"+"\StuPro_vfx_WS20\_PROD",'USER')

# update Syntheyes License
$License = "20,42,00,c4,a0,fa,89,76,43,69,e2,f7,f6,7b,ce,f7,a7,76,e0,f3,a3,f5,02,90,7d,45,5e,6a,ed,27,3d,44,33,db,8f,6f,af,11,26,56,d8,83,bd,bb,9e,93,0d,eb,b8,62,93,53,0a,3f,68,06,92,f3,af,29,2b,f2,42,db,b3,46,27,57,c2,29,62,2b,45,07,e9,04,c7,8e,d8,10,b3,d6,22,d1,e5,b5,1e,bf"
$RegPath   = 'HKLM:\Software\SynthEyes'
$AttrName  = "Auth14"

$hexified = $License.Split(',') | % { "0x$_"}
Set-ItemProperty -Path $RegPath -Name $AttrName -Value ([byte[]]$hexified)
# ---end update Syntheyes License


Write-EventLog -EventId 2404 -LogName Application -Message "Lovebirds_Env Script executed" -Source PetersAdminScript