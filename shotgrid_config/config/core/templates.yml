# Copyright (c) 2015 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

#
# This file is one of the central points in the Shotgun Pipeline Toolkit configuration and
# a counterpart to the folder configuration structure.
#
# The folder structure underneath the project folder is used to create folders on disk -
# templates.yml (this file) refers to those folders. Therefore, the two files need to be
# in sync. This file contains an overview of all locations that are used by Sgtk.
#
# Whenever an app or an engine refers to a location on disk, it is using an entry defined in
# this file. For more information, see the Shotgun Pipeline Toolkit Documentation.



#
# The keys section contains the definitions for all the different keys that are being
# used by Sgtk. A key is a magic token that is replaced by a value at runtime, for example
# {Shot}. The section below tells Sgtk which data types and formats to expect for each key.
#
keys:
    Sequence:
        type: str
    Shot:
        type: str
    Step:
        type: str
    sg_asset_type:
        type: str
    Asset:
        type: str
    name:
        type: str
        filter_by: alphanumeric
    iteration:
        type: int
    version:
        type: int
        format_spec: "03"
    version_four:
       type: int
       format_spec: "04"
       alias: version
    timestamp:
        type: str
    width:
        type: int
    height:
        type: int
    segment_name:
        type: str

    # Represents the optional output name for frames written by the Shotgun Write Node
    nuke.output:
        alias: output
        type: str
        filter_by: alphanumeric

    # Represents the optional output name for frames written by the After Effects Publish Rendering Plugin
    afx.comp:
        alias: comp
        type: str
        filter_by: alphanumeric
    afx.mov.ext:
        alias: extension
        type: str
        choices:
            mov: Quicktime Movie (.mov)
            avi: Audio Video Interleaved (.avi)

    SEQ:
        type: sequence
        format_spec: "04"

    # Represents a frame sequence exported from Flame
    flame.frame:
        type: sequence
        format_spec: "08"

    eye:
        type: str

    houdini.node:
        alias: node
        type: str
    aov_name:
        type: str

    # these are used by the Hiero exporter and pipeline
    YYYY:
        type: int
        format_spec: "04"
    MM:
        type: int
        format_spec: "02"
    DD:
        type: int
        format_spec: "02"
        alias: DD
    project:
        type: str

    # These are used for the Mari UDIM pipeline:
    UDIM:
        type: sequence
        default: "<UDIM>"
    mari.channel:
        type: str
        alias: channel
    mari.layer:
        type: str
        alias: layer
    mari.project_name:
        type: str
        alias: name
    asset_name:
        type: str
        shotgun_entity_type: Asset
        shotgun_field_name: code
    task_name:
        type: str
        shotgun_entity_type: Task
        shotgun_field_name: content

    # Maya supports two extension types.
    maya_extension:
        type: str
        choices:
            ma: Maya Ascii (.ma)
            mb: Maya Binary (.mb)
        default: ma
        alias: extension

    # needed by the Alias import as reference functionality
    alias.extension:
        type: str
        filter_by: alphanumeric

    # represents the optional render pass for frames written by VRED
    vred.render_pass:
        type: str
        filter_by: alphanumeric

    # represents the image extension for frames written by VRED
    vred.render_extension:
        type: str
        choices:
            png: PNG Image
            exr: EXR Image
        default: png

    # Represents a frame sequence exported from VRED
    vred.frame:
        type: sequence
        format_spec: "05"
        alias: SEQ


    ## CUSTOM
    username:
        type: str
        shotgun_entity_type: HumanUser
        shotgun_field_name: login
        length: 5

    shot_writeCategories:
        type: str
        choices: ["img-int", "img-fin", "img-cg", "mov", "img-jpg"]

    asset_writeCategories:
        type: str
        choices: ["img-int", "img-fin", "img-cg", "mov", "img-jpg"]

    # Substance Painter
    texture_extension:
        type: str
        alias: extension

    texture_name:
        type: str

#
# The paths section contains all the the key locations where files are to be stored
# by Sgtk Apps. Each path is made up of several keys (like {version} or {shot}) and
# these are defined in the keys section above.
#
# Apps use these paths as part of their configuration to define where on disk
# different files should go.
#

paths:

    # Common  path definitions to use as shorthand in order to avoid repetitive and verbose
    # templates. This also makes it easy to change any of the common root paths and have the
    # changes apply to all templates that use them.
    #
    # These don't require the standard formatting (with definition and root_name) because they
    # will be set within each template that uses the alias.
    shot_root: _PROD/_SHOTS/{Shot}
    asset_root: _PROD/_ASSETS/{sg_asset_type}/{Asset}
    sequence_root: _PROD/_SEQ/{Sequence}

    ##########################################################################################
    # Project level paths
    #

    #
    # Hiero
    #

    # The location of WIP files
    hiero_project_work:
        definition: '_PROD/_EDIT/work/{Step}/hiero/{Shot}_{task_name}_v{version}_{username}.hrox'
    hiero_project_work_area:
        definition: '_PROD/_EDIT/work'
    # The location of backups of WIP files
    hiero_project_snapshot:
        definition: '_PROD/_EDIT/work/{Step}/hiero/snapshots/{Shot}_{task_name}_v{version}_{username}_{timestamp}.hrox'
    # The location of published hiero files
    hiero_project_publish:
        definition: '_PROD/_EDIT/publish/{Shot}_{task_name}_v{version}_{username}.hrox'
    hiero_project_publish_area:
        definition: '_PROD/_EDIT/publish'


    ##########################################################################################
    # Sequence level paths
    #


    ##########################################################################################
    # Shot level paths
    #

    #
    # Flame
    #

    flame_segment_clip:
        definition: '_PROD/_SEQ/{Sequence}/{Shot}/finishing/clip/sources/{segment_name}.clip'
    flame_shot_clip:
        definition: '_PROD/_SEQ/{Sequence}/{Shot}/finishing/clip/{Shot}.clip'
    flame_shot_batch:
        definition: '_PROD/_SEQ/{Sequence}/{Shot}/finishing/batch/{Shot}_v{version}_{username}.batch'
    flame_shot_render_dpx:
        definition: '_SEQ/{Sequence}/{Shot}/finishing/renders/{segment_name}_v{version}/{Shot}_{segment_name}_v{version}_{username}.{flame.frame}.dpx'
    flame_shot_render_exr:
        definition: '_PROD/_SEQ/{Sequence}/{Shot}/finishing/renders/{segment_name}_v{version}/{Shot}_{segment_name}_v{version}_{username}.{flame.frame}.exr'
    flame_shot_comp_dpx:
        definition: '_PROD/_SEQ/{Sequence}/{Shot}/finishing/comp/{segment_name}_v{version}/{Shot}_{segment_name}_v{version}_{username}.{flame.frame}.dpx'
    flame_shot_comp_exr:
        definition: '_PROD/_SEQ/{Sequence}/{Shot}/finishing/comp/{segment_name}_v{version}/{Shot}_{segment_name}_v{version}_{username}.{flame.frame}.exr'

    #
    # Photoshop
    #

    # The location of WIP files
    photoshop_shot_work:
        definition: '@shot_root/2D/{Step}/work/photoshop/{Shot}_{task_name}_v{version}_{username}.psd'
    shot_work_area_photoshop:
        definition: '@shot_root/2D/{Step}/work/photoshop'
    # The location of backups of WIP files
    photoshop_shot_snapshot:
        definition: '@shot_root/2D/{Step}/work/photoshop/snapshots/{Shot}_{task_name}_v{version}_{username}.{timestamp}.psd'
    # The location of published files
    shot_publish_area_photoshop:
        definition: '@shot_root/2D/{Step}/publish/photoshop'
    photoshop_shot_publish:
        definition: '@shot_root/2D/{Step}/publish/photoshop/{Shot}_{task_name}_v{version}_{username}.psd'

    #
    # After Effects
    #

    # The location of WIP files
    aftereffects_shot_work:
        definition: '@shot_root/2D/{Step}/work/afx/{Shot}_{task_name}_v{version}_{username}.aep'
    shot_work_area_aftereffects:
        definition: '@shot_root/2D/{Step}/work/afx'
    # The location of backups of WIP files
    aftereffects_shot_snapshot:
        definition: '@shot_root/2D/{Step}/work/afx/snapshots/{Shot}_{task_name}_v{version}_{username}.{timestamp}.aep'
    # The location of published files
    shot_publish_area_aftereffects:
        definition: '@shot_root/2D/{Step}/publish/afx'
    aftereffects_shot_publish:
        definition: '@shot_root/2D/{Step}/publish/afx/{Shot}_{task_name}_v{version}_{username}.aep'
    aftereffects_shot_render_pub_mono:
        definition: '@shot_root/2D/{Step}/publish/elements/{Shot}/v{version}/{width}x{height}/{Shot}_{task_name}_{name}_{afx.comp}_v{version}_{username}.{SEQ}.tif'
    # The following template uses {afx.mov.ext} this is a special key, that will be only there
    # in the beta to support different extensions on mac and windows, while using the same
    # output module (Lossless with Alpha)
    aftereffects_shot_render_movie:
        definition: '@shot_root/2D/{Step}/review/{Shot}_{task_name}_{name}_{afx.comp}_v{version}_{username}.{afx.mov.ext}'


    #
    # Maya
    #

    # define the location of a work area
    shot_work_area_maya:
        definition: '@shot_root/3D/{Step}/work/maya'
    # define the location of a publish area
    shot_publish_area_maya:
        definition: '@shot_root/3D/{Step}/publish/maya'
    # The location of WIP files
    maya_shot_work:
        definition: '@shot_root/3D/{Step}/work/maya/{Shot}_{task_name}_v{version}_{username}.{maya_extension}'
    # The location of backups of WIP files
    maya_shot_snapshot:
        definition: '@shot_root/3D/{Step}/work/maya/snapshots/{Shot}_{task_name}_v{version}_{username}.{timestamp}.{maya_extension}'
    # The location of published maya files
    maya_shot_publish:
        definition: '@shot_root/3D/{Step}/publish/maya/{Shot}_{task_name}_v{version}_{username}.{maya_extension}'
    # The location of maya playblasts
    maya_shot_playblast:
        definition: '@shot_root/3D/{Step}/_export/img-prv/{Shot}_{Step}_playblast_v{version}_{username}.avi'


    #
    # Houdini
    #

    # define the location of a work area
    shot_work_area_houdini:
        definition: '@shot_root/3D/{Step}/work/houdini'
    # define the location of a publish area
    shot_publish_area_houdini:
        definition: '@shot_root/3D/{Step}/publish/houdini'
    # The location of WIP files
    houdini_shot_work:
        definition: '@shot_root/3D/{Step}/work/houdini/{Shot}_{task_name}_v{version}_{username}.hipnc'
    # The location of backups of WIP files
    houdini_shot_snapshot:
        definition: '@shot_root/3D/{Step}/work/houdini/snapshots/{Shot}_{task_name}_v{version}_{username}.{timestamp}.hipnc'
    # The location of published houdini files
    houdini_shot_publish:
        definition: '@shot_root/3D/{Step}/publish/houdini/{Shot}_{task_name}_v{version}_{username}.hipnc'
    # Alembic caches
    houdini_shot_work_alembic_cache:
        definition: '@shot_root/3D/{Step}/work/houdini/cache/alembic/{Shot}/{houdini.node}/v{version}/{Shot}_{task_name}_{name}_v{version}_{username}.abc'

    # Rendered images
    houdini_shot_render:
        definition: '@shot_root/3D/{Step}/work/images/{Shot}/{houdini.node}/v{version}/{width}x{height}/{Shot}_{task_name}_{name}_v{version}_{username}.{SEQ}.exr'

    # Additional mantra outputs
    houdini_shot_ifd:
        definition: '@shot_root/3D/{Step}/work/ifds/{Shot}/{houdini.node}/v{version}/{width}x{height}/{Shot}_{task_name}_{name}_v{version}_{username}.{SEQ}.ifd'

    houdini_shot_dcm:
        definition: '@shot_root/3D/{Step}/work/dcms/{Shot}/{houdini.node}/v{version}/{width}x{height}/{Shot}_{task_name}_{name}_v{version}_{username}.{SEQ}.dcm'

    houdini_shot_extra_plane:
        definition: '@shot_root/3D/{Step}/work/images/{Shot}/{houdini.node}/{aov_name}/v{version}/{width}x{height}/{Shot}_{task_name}_{name}_v{version}_{username}.{SEQ}.exr'


    #
    # 3dsmax
    #

    # define the location of a work area
    shot_work_area_max:
        definition: '@shot_root/3D/{Step}/work/3dsmax'
    # define the location of a publish area
    shot_publish_area_max:
        definition: '@shot_root/3D/{Step}/publish/3dsmax'
    # The location of WIP files
    max_shot_work:
        definition: '@shot_root/3D/{Step}/work/3dsmax/{Shot}_{task_name}_v{version}_{username}.max'
    # The location of backups of WIP files
    max_shot_snapshot:
        definition: '@shot_root/3D/{Step}/work/3dsmax/snapshots/{Shot}_{task_name}_v{version}_{username}.{timestamp}.max'
    # The location of published max files
    max_shot_publish:
        definition: '@shot_root/3D/{Step}/publish/3dsmax/{Shot}_{task_name}_v{version}_{username}.max'


    #
    # Motionbuilder
    #

    # define the location of a work area
    shot_work_area_mobu:
        definition: '@shot_root/3D/{Step}/work/mobu'
    # define the location of a publish area
    shot_publish_area_mobu:
        definition: '@shot_root/3D/{Step}/publish/mobu'
    # The location of WIP files
    mobu_shot_work:
        definition: '@shot_root/3D/{Step}/work/mobu/{Shot}_{task_name}_v{version}_{username}.fbx'
    # The location of backups of WIP files
    mobu_shot_snapshot:
        definition: '@shot_root/3D/{Step}/work/mobu/snapshots/{Shot}_{task_name}_v{version}_{username}.{timestamp}.fbx'
    # The location of published mobu files
    mobu_shot_publish:
        definition: '@shot_root/3D/{Step}/publish/mobu/{Shot}_{task_name}_v{version}_{username}.fbx'


    #
    # Nuke
    #

    # define the location of a work area
    shot_work_area_nuke:
        definition: '@shot_root/2D/{Step}/work/nuke'
    # define the location of a publish area
    shot_publish_area_nuke:
        definition: '@shot_root/2D/{Step}/publish/nuke'
    # The location of WIP script files
    nuke_shot_work:
        definition: '@shot_root/2D/{Step}/work/nuke/{Shot}_{Step}_v{version}_{username}.nk'
    # The location of backups of WIP files
    nuke_shot_snapshot:
        definition: '@shot_root/2D/{Step}/work/nuke/snapshots/{Shot}_{Step}_v{version}_{username}.{timestamp}.nk'
    # The location of published nuke script files
    nuke_shot_publish:
        definition: '@shot_root/2D/{Step}/publish/nuke/{Shot}_{Step}_v{version}_{username}.nk'
    # write node outputs
    nuke_shot_render_mono_dpx:
        definition: '@shot_root/2D/{Step}/work/images/{Shot}/v{version}/{width}x{height}/{Shot}_{Step}_{name}_{nuke.output}_v{version}_{username}.{SEQ}.dpx'
    nuke_shot_render_pub_mono_dpx:
        definition: '@shot_root/2D/{Step}/publish/elements/{Shot}/v{version}/{width}x{height}/{Shot}_{Step}_{name}_{nuke.output}_v{version}_{username}.{SEQ}.dpx'
    nuke_shot_render_stereo:
        definition: '@shot_root/2D/{Step}/work/images/{Shot}/v{version}/{width}x{height}/{Shot}_{Step}_{name}_{nuke.output}_{eye}_v{version}_{username}.{SEQ}.exr'
    nuke_shot_render_pub_stereo:
        definition: '@shot_root/2D/{Step}/publish/elements/{Shot}/v{version}/{width}x{height}/{Shot}_{Step}_{name}_{nuke.output}_{eye}_v{version}_{username}.{SEQ}.exr'
    # review output
    shot_quicktime_quick:
        definition: '@shot_root/2D/{Step}/review/quickdaily/{Shot}_{Step}_{name}_{iteration}.mov'
    nuke_shot_render_movie:
        definition: '@shot_root/2D/{Step}/review/{Shot}_{Step}_{name}_{nuke.output}_v{version}_{username}.mov'

    #
    # Hiero
    #

    # export of shot asset data from hiero
    hiero_plate_path:
        definition: '_PROD/_SEQ/{Sequence}/{Shot}/editorial/{YYYY}_{MM}_{DD}/plates/{project}_{Shot}.mov'
    hiero_render_path:
        definition: '_PROD/_SEQ/{Sequence}/{Shot}/editorial/{YYYY}_{MM}_{DD}/renders/{project}_{Shot}.{SEQ}.dpx'

    ##########################################################################################
    # Asset pipeline


    #
    # Alembic caches
    #

    asset_alembic_cache:
        definition: '@asset_root/publish/{Step}/caches/{Asset}_{task_name}_v{version}_{username}.abc'


    #
    # Photoshop
    #

    # The location of WIP files
    photoshop_asset_work:
        definition: '@asset_root/work/{Step}/photoshop/{Asset}_{task_name}_v{version}_{username}.psd'
    asset_work_area_photoshop:
        definition: '@asset_root/work/{Step}/photoshop'
    # The location of backups of WIP files
    photoshop_asset_snapshot:
        definition: '@asset_root/work/{Step}/photoshop/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.psd'
    # The location of published files
    asset_publish_area_photoshop:
        definition: '@asset_root/publish/{Step}/photoshop'
    photoshop_asset_publish:
        definition: '@asset_root/publish/{Step}/photoshop/{Asset}_{task_name}_v{version}_{username}.psd'

    #
    # after effects
    #

    # The location of WIP files
    aftereffects_asset_work:
        definition: '@asset_root/work/{Step}/afx/{Asset}_{task_name}_v{version}_{username}.aep'
    asset_work_area_aftereffects:
        definition: '@asset_root/work/{Step}/afx'
    # The location of backups of WIP files
    aftereffects_asset_snapshot:
        definition: '@asset_root/work/{Step}/afx/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.aep'
    # The location of published files
    asset_publish_area_aftereffects:
        definition: '@asset_root/publish/{Step}/afx'
    aftereffects_asset_publish:
        definition: '@asset_root/publish/{Step}/afx/{Asset}_{task_name}_v{version}_{username}.aep'
    aftereffects_asset_render_pub_mono:
        definition: '@asset_root/publish/{Step}/elements/{Asset}/v{version}/{width}x{height}/{Asset}_{task_name}_{name}_{afx.comp}_v{version}_{username}.{SEQ}.tif'
    # The following template uses {afx.mov.ext} this is a special key, that will be only there
    # in the beta to support different extensions on mac and windows, while using the same
    # output module (Lossless with Alpha)
    aftereffects_asset_render_movie:
        definition: '@asset_root/review/{Asset}_{task_name}_{name}_{afx.comp}_v{version}_{username}.{afx.mov.ext}'


    #
    # Mari
    #
    asset_mari_texture_tif:
        definition: '@asset_root/publish/{Step}/mari/{Asset}_{task_name}_{mari.channel}[_{mari.layer}]_v{version}_{username}.{UDIM}.tif'

    #
    # Maya
    #

    # define the location of a work area
    asset_work_area_maya:
        definition: '@asset_root/work/{Step}/maya'
    # define the location of a publish area
    asset_publish_area_maya:
        definition: '@asset_root/publish/{Step}/maya'
    # The location of WIP files
    maya_asset_work:
        definition: '@asset_root/work/{Step}/maya/{Asset}_{task_name}_v{version}_{username}.{maya_extension}'
    # The location of backups of WIP files
    maya_asset_snapshot:
        definition: '@asset_root/work/{Step}/maya/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.{maya_extension}'
    # The location of published maya files
    maya_asset_publish:
        definition: '@asset_root/publish/{Step}/maya/{Asset}_{task_name}_v{version}_{username}.{maya_extension}'
    # The location of maya playblasts
    maya_asset_playblast:
        definition: '@asset_root/_export/img-prv/{Asset}_{Step}_playblast}_v{version}_{username}.avi'


    #
    # Houdini
    #

    # define the location of a work area
    asset_work_area_houdini:
        definition: '@asset_root/work/{Step}/houdini'
    # define the location of a publish area
    asset_publish_area_houdini:
        definition: '@asset_root/publish/{Step}/houdini'
    # The location of WIP files
    houdini_asset_work:
        definition: '@asset_root/work/{Step}/houdini/{Asset}_{task_name}_v{version}_{username}.hipnc'
    # The location of backups of WIP files
    houdini_asset_snapshot:
        definition: '@asset_root/work/{Step}/houdini/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.hipnc'
    # The location of published houdini files
    houdini_asset_publish:
        definition: '@asset_root/publish/{Step}/houdini/{Asset}_{task_name}_v{version}_{username}.hipnc'
    # Alembic caches
    houdini_asset_work_alembic_cache:
        definition: '@asset_root/work/{Step}/houdini/cache/alembic/{Asset}/{houdini.node}/v{version}/{Asset}_{task_name}_{name}_v{version}_{username}.abc'

    # Rendered images
    houdini_asset_render:
        definition: '@asset_root/work/{Step}/images/{Asset}/{houdini.node}/v{version}/{width}x{height}/{Asset}_{task_name}_{name}_v{version}_{username}.{SEQ}.exr'

    # Additional mantra outputs
    houdini_asset_ifd:
        definition: '@asset_root/work/{Step}/ifds/{Asset}/{houdini.node}/v{version}/{width}x{height}/{Asset}_{task_name}_{name}_v{version}_{username}.{SEQ}.ifd'

    houdini_asset_dcm:
        definition: '@asset_root/work/{Step}/dcms/{Asset}/{houdini.node}/v{version}/{width}x{height}/{Asset}_{task_name}_{name}_v{version}_{username}.{SEQ}.dcm'

    houdini_asset_extra_plane:
        definition: '@asset_root/work/{Step}/images/{Asset}/{houdini.node}/{aov_name}/v{version}/{width}x{height}/{Asset}_{task_name}_{name}_v{version}_{username}.{SEQ}.exr'


    #
    # 3dsmax
    #

    # define the location of a work area
    asset_work_area_max:
        definition: '@asset_root/work/{Step}/3dsmax'
    # define the location of a publish area
    asset_publish_area_max:
        definition: '@asset_root/publish/{Step}/3dsmax'
    # The location of WIP files
    max_asset_work:
        definition: '@asset_root/work/{Step}/3dsmax/{Asset}_{task_name}_v{version}_{username}.max'
    # The location of backups of WIP files
    max_asset_snapshot:
        definition: '@asset_root/work/{Step}/3dsmax/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.max'
    # The location of published max files
    max_asset_publish:
        definition: '@asset_root/publish/{Step}/3dsmax/{Asset}_{task_name}_v{version}_{username}.max'


    #
    # Motionbuilder
    #

    # define the location of a work area
    asset_work_area_mobu:
        definition: '@asset_root/work/{Step}/mobu'
    # define the location of a publish area
    asset_publish_area_mobu:
        definition: '@asset_root/publish/{Step}/mobu'
    # The location of WIP files
    mobu_asset_work:
        definition: '@asset_root/work/{Step}/mobu/{Asset}_{task_name}_v{version}_{username}.fbx'
    # The location of backups of WIP files
    mobu_asset_snapshot:
        definition: '@asset_root/work/{Step}/mobu/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.fbx'
    # The location of published Motionbuilder files
    mobu_asset_publish:
        definition: '@asset_root/publish/{Step}/mobu/{Asset}_{task_name}_v{version}_{username}.fbx'


    #
    # Nuke
    #

    # define the location of a work area
    asset_work_area_nuke:
        definition: '@asset_root/work/{task_name}/nuke'
    # define the location of a publish area
    asset_publish_area_nuke:
        definition: '@asset_root/publish/{task_name}/nuke'
    # outputs from the Shotgun Write Node for assets
    nuke_asset_render:
        definition: '@asset_root/work/{task_name}/images/{Asset}/v{version}/{width}x{height}/{Asset}_{task_name}_{name}_{nuke.output}_v{version}_{username}.{SEQ}.exr'
    nuke_asset_render_pub:
        definition: '@asset_root/elements/{Asset}/v{version}/{width}x{height}/{Asset}_{task_name}_{name}_{nuke.output}_v{version}_{username}.{SEQ}.exr'
    # review output
    nuke_asset_render_movie:
        definition: '@asset_root/review/{Asset}_{task_name}_{name}_{nuke.output}_v{version}_{username}.mov'
    asset_quicktime_quick:
        definition: '@asset_root/review/quickdaily/{Asset}_{task_name}_{name}_{iteration}_{username}.mov'
    # The location of WIP script files
    nuke_asset_work:
        definition: '@asset_root/work/{task_name}/nuke/{Asset}_{task_name}_v{version}_{username}.nk'
    # The location of backups of WIP files
    nuke_asset_snapshot:
        definition: '@asset_root/work/{task_name}/nuke/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.nk'
    # The location of published nuke script files
    nuke_asset_publish:
        definition: '@asset_root/publish/{task_name}/nuke/{Asset}_{task_name}_v{version}_{username}.nk'

    #
    # Alias
    #

    # define the location of a work area
    asset_work_area_alias:
        definition: '@asset_root/work/{Step}/alias'
    # define the location of a publish area
    asset_publish_area_alias:
        definition: '@asset_root/publish/{Step}/alias'
    # The location of WIP files
    alias_asset_work:
        definition: '@asset_root/work/{Step}/alias/{Asset}_{task_name}_v{version}_{username}.wire'
    # The location of backups of WIP files
    alias_asset_snapshot:
        definition: '@asset_root/work/{Step}/alias/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.wire'
    # The location of published Alias files
    alias_asset_publish:
        definition: '@asset_root/publish/{Step}/alias/{Asset}_{task_name}_v{version}_{username}.wire'

    # Alias translations

    # The location of the reference created on the fly by Alias when importing a file as ref
    alias_asset_reference_publish:
        definition: '@asset_root//publish/{Step}/alias/translations/{Asset}_{task_name}_{alias.extension}_v{version}_{username}.wref'
    alias_asset_igs_publish:
        definition: '@asset_root/publish/{Step}/alias/translations/{Asset}_{task_name}_v{version}_{username}.igs'
    alias_asset_catpart_publish:
        definition: '@asset_root/publish/{Step}/alias/translations/{Asset}_{task_name}_v{version}_{username}.CATPart'
    alias_asset_jt_publish:
        definition: '@asset_root/publish/{Step}/alias/translations/{Asset}_{task_name}_v{version}_{username}.jt'
    alias_asset_stp_publish:
        definition: '@asset_root/publish/{Step}/alias/translations/{Asset}_{task_name}_v{version}_{username}.stp'
    alias_asset_wref_publish:
        definition: '@asset_root/publish/{Step}/alias/translations/{Asset}_{task_name}_v{version}_{username}.wref'

    #
    # VRED
    #

    # define the location of a work area
    asset_work_area_vred:
        definition: '@asset_root/work/{Step}/vred'
    # define the location of a publish area
    asset_publish_area_vred:
        definition: '@asset_root/publish/{Step}/vred'
    # The location of WIP files
    vred_asset_work:
        definition: '@asset_root/work/{Step}/vred/{Asset}_{task_name}_v{version}_{username}.vpb'
    # The location of backups of WIP files
    vred_asset_snapshot:
        definition: '@asset_root/work/{Step}/vred/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.vpb'
    # The location of published VRED files
    vred_asset_publish:
        definition: '@asset_root/publish/{Step}/vred/{Asset}_{task_name}_v{version}_{username}.vpb'

    # define the location of the WIP render images
    vred_asset_render_work:
        definition: '@asset_root/work/{Step}/images/{Asset}/v{version}/{Asset}_{task_name}_{name}_v{version}_{username}[-{vred.render_pass}].{vred.render_extension}'

    # define the location of the WIP renderings for an image sequence
    vred_asset_render_sequence_work:
        definition: '@asset_root/work/{Step}/images/{Asset}/v{version}/{Asset}_{task_name}_{name}_v{version}_{username}[-{vred.render_pass}]-{vred.frame}.{vred.render_extension}'

    # define the location of the published render images
    vred_asset_render_publish:
        definition: '@asset_root/publish/{Step}/images/{Asset}/v{version}/{Asset}_{task_name}_{name}_v{version}_{username}[-{vred.render_pass}].{vred.render_extension}'

    # define the location of the published renderings for an image sequence
    vred_asset_render_sequence_publish:
        definition: '@asset_root/publish/{Step}/images/{Asset}/v{version}/{Asset}_{task_name}_{name}_v{version}_{username}[-{vred.render_pass}]-{vred.frame}.{vred.render_extension}'


    # Substance Painter

    # asset
    # define the location of a work area
    asset_work_area_substancepainter:
        definition: '@asset_root/work/{Step}/substancepainter'
        root_name: 'primary'
    # define the location of a publish area
    asset_publish_area_substancepainter:
        definition: '@asset_root/publish/{Step}/substancepainter'
        root_name: 'primary'

    # The location of WIP files
    substancepainter_asset_work:
        definition: '@asset_work_area_substancepainter/{Asset}_{task_name}_v{version}_{username}.spp'
        root_name: 'primary'

    # The location of backups of WIP files
    substancepainter_asset_snapshot:
        definition: '@asset_work_area_substancepainter/snapshots/{Asset}_{task_name}_v{version}_{username}.{timestamp}.spp'
        root_name: 'primary'

    # The folder where the textures are exported for a project
    substancepainter_asset_textures_path_export:
        definition: '@asset_work_area_substancepainter/_export/img-cg'
        root_name: 'primary'

    # The location of published substance painter files
    substancepainter_asset_publish:
        definition: '@asset_publish_area_substancepainter/{Asset}_{task_name}_v{version}_{username}.spp'
        root_name: 'primary'

    # a texture folder publish
    substancepainter_asset_textures_path_publish:
        definition: '@asset_publish_area_substancepainter/textures/{Asset}_{task_name}_textures_v{version}_{username}'
        root_name: 'primary'

    # a texture folder publish
    substancepainter_asset_texture_path_publish:
        definition: '@asset_publish_area_substancepainter/textures/{Asset}_{task_name}_{texture_name}_v{version}_{username}.{texture_extension}'
        root_name: 'primary'

#
# The strings section is similar to the paths section - but rather than defining paths
# on disk, it contains a list of strings. Strings are typically used when you want to be
# able to configure the way data is written to shotgun - it may be the name field for a
# review version or the formatting of a publish.
#

strings:

    # when a review Version in Shotgun is created inside of Nuke, this is the
    # name that is being given to it (the code field)
    nuke_shot_version_name: "{Shot}_{name}_{nuke.output}_v{version}.{iteration}"
    nuke_quick_shot_version_name: "{Shot}_{name}_quick_{iteration}"

    nuke_asset_version_name: "{Asset}_{name}_{nuke.output}_v{version}.{iteration}"
    nuke_quick_asset_version_name: "{Asset}_{name}_quick_{iteration}"

    # defines how the {tk_version} token in Hiero gets formatted back to tk.
    hiero_version: "{version}"

    # define how new Mari projects should be named
    mari_asset_project_name: "{mari.project_name} - Asset {asset_name}, {task_name}"
